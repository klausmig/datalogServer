Git global setup

git config --global user.name "Klaus Martinez Hollaender"
git config --global user.email "klausmig@gmail.com"

Create a new repository

git clone https://gitlab.com/klausmig/datalogServer.git
cd datalogServer
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin https://gitlab.com/klausmig/datalogServer.git
git add .
git commit
git push -u origin master