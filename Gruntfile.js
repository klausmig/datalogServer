module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		jsbeautifier: {
			files: ['direct/**/*.js', 'src/main/resources/**/*.tml', 'src/main/resources/net/netm/platform/client/tapestry/mediathek/lib/js/thumbnail-slider.js'],
			options: {
				//config: "path/to/configFile",
				html: {
					fileTypes: [".tml"],
					braceStyle: "collapse",
					indentChar: " ",
					indentScripts: "keep",
					indentSize: 4,
					maxPreserveNewlines: 1,
					preserveNewlines: false,
					unformatted: ["a", "sub", "sup", "b", "i", "u"],
					wrapLineLength: 0
				},
				css: {
					indentChar: " ",
					indentSize: 4
				},
				js: {
					fileTypes: [".java"],
					braceStyle: "collapse",
					breakChainedMethods: false,
					e4x: false,
					evalCode: false,
					indentChar: " ",
					indentLevel: 0,
					indentSize: 4,
					indentWithTabs: false,
					jslintHappy: false,
					keepArrayIndentation: true,
					keepFunctionIndentation: false,
					maxPreserveNewlines: 2,
					preserveNewlines: true,
					spaceBeforeConditional: true,
					spaceInParen: false,
					unescapeStrings: false,
					wrapLineLength: 0,
					endWithNewline: true
				}
			}
		},


		ts: {
			default: {
				src: ["ts/datalog.ts"],
				outDir: "ts/",

			},
			options: {
				fast: 'never',
				module: "commonjs",
				target: "es6",
				sourceMap: true,
			}
		},



		babel: {
			options: {
				plugins: ['transform-es2015-modules-amd'
					/*, 
									          'transform-remove-console'*/
				],
				presets: ['es2015']
			},

			dist: {
				files: [{
					expand: true,
					cwd: 'src/main/webapp/js/scripts',
					src: ['**/*.js'],
					dest: 'src/main/webapp/js/tmp',
					ext: '.js'
				}]
			}
		},

		concat: {
			options: {

			},
			dist: {
				options: {

				},
				src: [
					'ts/src/term.ts ',
					'ts/src/constant.ts',
					'ts/src/variable.ts',
					'ts/src/match.ts',
					'ts/src/literal.ts',
					'ts/src/rule.ts',
					'ts/src/sipsrule.ts',
					'ts/src/database.ts',
					'ts/src/lexer.ts',
					'ts/src/dependencygraph.ts',
					'ts/src/parser.ts',
					'ts/src/answerliteral.ts',
					'ts/src/queryliteral.ts',
					'ts/src/sips.ts',
					'ts/src/magicrule.ts',
					'ts/src/magicset.ts',
					'ts/src/footer-export.ts',
					//'src/main/webapp/js/tmp/<%= pkg.name %>.js'


				],
				dest: 'ts/datalog.ts'
			}
		},


		uglify: {
			dist: {
				src: 'src/main/webapp/dist/<%= pkg.name %>.js',
				dest: 'src/main/webapp/dist/<%= pkg.name %>.min.js'
			},
			options: {
				compress: {
					drop_console: true
				}
			},
		},

		requirejs: {
			compile: {
				options: {
					baseUrl: 'src/main/webapp/js/tmp',
					include: ['app.js'],
					out: 'src/main/webapp/js/tmp/<%= pkg.name %>.js'
				}
			}
		},

		clean: ['src/main/webapp/js/tmp'],

		sass: {
			dist: {
				options: {
					outputStyle: 'compressed',
					sourceMap: true
				},
				files: {
					'src/main/resources/net/netm/platform/client/tapestry/mediathek/css/<%= pkg.name %>.css': 'src/main/resources/net/netm/platform/client/tapestry/mediathek/sass/style.scss'
				}

			}
		},

		watch: {
			layout: {
				files: ['src/main/resources/net/netm/platform/client/tapestry/mediathek/sass/**/*.scss'],
				tasks: ['layout']
			},
			js: {
				files: ['ts/src/*.ts'],
				tasks: ['js']
			}
			// ,
			// beautify:{
			// 	files: ['src/main/java/**/*.java','src/main/resources/**/*.tml','src/main/resources/net/netm/platform/client/tapestry/mediathek/lib/js/thumbnail-slider.js'],
			// 	tasks: ['beautify-all']
			// }
		}
	});

	// JS tasks
	grunt.registerTask('js', ['concat:dist', 'ts']);

	// Typescript tasks
	grunt.registerTask('type', ['ts']);


	// CSS/SASS tasks
	grunt.registerTask('layout', ['sass', 'jsbeautifier']);

	//tml
	grunt.registerTask('beautify-all', ['jsbeautifier']);

	// NPM Build task
	grunt.registerTask('build', ['js', 'sass']);

	// Expose the default watch task
	grunt.registerTask('default', ['watch']);
};