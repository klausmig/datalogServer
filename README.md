# DatalogServer

## Installation


- Install node.js v8.4.0+ from https://nodejs.org/en/ 
- Install Typescript with the command: **npm install -g typescript**
- Clone this project in htdocs/DatalogClient with **git clone git@gitlab.com:klausmig/datalogServer.git**
- Inside the folder project execute: **npm install**
- Import the initial db schema into the MySql Server from the file **schema-datalog-mysql.sql**,
- In apache config change the values: **LimitRequestFieldSize 100000** and **LimitRequestLine 100000**
- In order to run the node server execute : **node server.js**


## Project Structure 

This folder is primarily a container for the top-level pieces of the application.
While you can remove some files and folders that this application does not use,
be sure to read below before deciding what can be deleted and what needs to be
kept in source control.

The following files are all needed to build and load the application.

 -  Folder `"direct"` contains all API rest classes.
 -  Folder `"ts/src"` contains all Datalog Manager classes.
 - `"db-config.json"` - config variables for MySQL server.
 - `"schema-datalog-mysql.sql"` - initial sample data with schema..
 - `"server.js"` - Main server file.



# Other Options

## Developmet

 -   Any change of the Datalog kernel located in the foleder ts/src can be automacally compiled using the command: 
     **grunt watch**
 -   Manually compilation of the typescript files located in the foleder ts/src,using the command: **grunt ts**
 -  Files compilation using only typescript, in the folder ts/src execute:

 **cat term.ts constant.ts variable.ts match.ts literal.ts rule.ts sipsrule.ts database.ts  lexer.ts dependencygraph.ts parser.ts answerliteral.ts queryliteral.ts sips.ts magicrule.ts magicset.ts footer-export.ts >  ../datalog.ts**

 **tsc --module "commonJs" --target "ES6" --declaration datalog.ts**




