var datalog = global.App.datalog;
var db = global.App.database.connection;

var Interpreter = {

    getSipsRules: function(params, callback) {

        console.log("Executing getSipsRules()...")

        // var sips = global.App.sipsRules;
        var magicSet = global.App.magicSet;

        var selectedRowsId = JSON.parse(params.selectedRowsId)

        var rules = []
        var counter = 0;

        if (selectedRowsId) {
            for (var num in selectedRowsId) {
                //console.log("numSIPS:" + (selectedRowsId[num] - 1));

                var index = selectedRowsId[num] - 1;
                var sips = magicSet[index].sipsRules;
                //console.log('sips:' + sips);
                for (var j = 0; j < sips.length; j++) {
                    counter++;
                    rules.push({
                        id: counter,
                        adorment: sips[j].adorment,
                        'sip-rule': sips[j].toString(),
                        rule: magicSet[index].rule.toString()
                    });
                }

            }
        } else {

            //console.log("\n\magicSet.sipsRules:\n" + magicSet.sipsRules);

            for (var i = 0; i < magicSet.length; i++) {
                var sips = magicSet[i].sipsRules;
                for (var j = 0; j < sips.length; j++) {
                    counter++;
                    rules.push({
                        id: counter,
                        adorment: sips[j].adorment,
                        'sip-rule': sips[j].toString(),
                        rule: magicSet[i].rule.toString()
                    });
                }
            }
        }

        callback(null, {
            data: rules,
            params: params
        })

    },

    getDirectedGraph: function(params, callback) {

        callback(null, {
            nodes: global.App.nodes,
            edges: global.App.edges
        })

    },

    getDirectedGraphMS: function(params, callback) {

        callback(null, {
            nodes: global.App.msnodes,
            edges: global.App.msedges
        })

    },

    getLog: function(params, callback) {

        var log = global.App.log;
        var msg = []
        for (var i = 0; i < log.length; i++) {

            msg.push({
                id: i + 1,
                log: log[i]
            });
        }

        //console.log("msg:" + msg);
        callback(null, {
            data: msg,
            params: params
        })

    },

    loadDb: function(params, callback) {

        if ((global.App.loadedDbId != params.db_id) || (params.toReload)) {

            datalog.log = [];
            global.App.magicSet = null;
            global.App.loadedDbId = null;
            global.App.loadedRules = null;
            global.App.loadedFacts = null;
            global.App.directGraph = null;

            var sqlRule = 'SELECT value as rule_value FROM db,rule WHERE db.id=id_db AND db.id = ' + params.db_id;
            var sqlFact = 'SELECT value as fact_value FROM db,fact WHERE db.id=id_db AND db.id = ' + params.db_id;

            db.query(sqlRule, function(err, rows, fields) {
                if (err) {
                    db.debug(err, callback);
                    return false;
                }

                var dbRules = '';

                for (var i = 0; i < rows.length; i++) {
                    dbRules += rows[i].rule_value;
                }

                //console.log('dbRules:' + dbRules);
                db.query(sqlFact, function(err, rowsFact, fields) {
                    if (err) {
                        db.debug(err, callback);
                        return false;
                    }

                    var dbFacts = '';

                    for (var i = 0; i < rowsFact.length; i++) {
                        dbFacts += rowsFact[i].fact_value;
                    }

                    //console.log('dbFacts:' + dbFacts);
                    var dbase;
                    var success = false;

                    var FactTokenList = datalog.lexer(dbFacts);
                    var RuleTokenList = datalog.lexer(dbRules);

                    var parserFactObj = new datalog.Parser(FactTokenList);
                    var parserRuleObj = new datalog.Parser(RuleTokenList);

                    facts = parserFactObj.parseRules();
                    rules = parserRuleObj.parseRules();


                    console.log("log:" + datalog.log);

                    if ((!parserFactObj.errorParser) && (!parserRuleObj.errorParser)) {


                        success = true;

                        //For genberatin direct graph          
                        parserRuleObj.dg.analyze();
                        parserFactObj.dg.analyze();

                        dbase = new datalog.Database(facts.concat(rules));
                        //if (!isDbLoaded) {
                        //console.log("DATABASE:\n" + dbase.toHtml());
                        datalog.log.push("<strong>Parsed and loaded Rules:</strong><br>" + dbase.toHtml());

                        var allNodes = parserRuleObj.dg.nodes.concat(parserFactObj.dg.nodes);
                        global.App.nodes = allNodes;
                        global.App.edges = parserRuleObj.dg.edges;

                        // console.log('global.App.nodes:' + global.App.nodes);
                        // console.log('global.App.edges:' + global.App.edges);

                        global.App.loadedDbId = params.db_id;
                        global.App.loadedRules = rules;
                        global.App.loadedFacts = facts;
                        global.App.directGraph = parserRuleObj.dg.directedGraph;

                    } else {

                        global.App.loadedDbId = null;
                        global.App.loadedRules = null;
                        global.App.loadedFacts = null;
                        global.App.directGraph = null;

                        var errMsg = "Factsed";

                        if (parserFactObj.errorParser)
                            errMsg = "Facts";


                        //console.log("log:" + datalog.log);

                        datalog.log.push('<div class="alert alert-danger"><strong> Parsing ' + errMsg + ' Error!</strong> </div>');

                    }

                    global.App.log = datalog.log;

                    callback(null, {
                        success: success,
                        isJustLoaded: true,
                        nodes: global.App.nodes,
                        edges: global.App.edges,
                        params: params
                    });
                });
            });
        } else {

            datalog.log.push('<strong>DB id:' + global.App.loadedDbId + ' already laoded!</strong>');

            callback(null, {
                success: true,
                isJustLoaded: false,
                msg: 'DB id:' + global.App.loadedDbId + ' already laoded!',
                params: params
            });
        }
    },

    getMagicRules: function(params, callback) {

        //datalog.log = [];

        var selectedRowsId = JSON.parse(params.selectedRowsId)

        console.log("Executing getMagicRules with selectedRowsId:" + selectedRowsId);
        //console.log(selectedRowsId);

        var dbase = new datalog.Database(global.App.loadedRules);
        //console.log("DATABASE:\n" + dbase);

        if ((params.generate) || (global.App.magicSet == null)) {

            console.log('params.generate:' + params.generate + " generating ms!");

            var magicObj = new datalog.MagicSet(dbase, global.App.directGraph);

            datalog.log.push("<strong>Rules to feed the Magic Sets Compiler:</strong><br>" + dbase.toHtml());

            magicObj.generateMS();

            // console.log("MS:\n" + magicObj);
            //console.log("magicObj.sipsList:\n" + magicObj.sipsList);
            global.App.magicSetStr = magicObj.toString();
            global.App.queryRules = magicObj.queryList;
            global.App.answerRules = magicObj.answerList;
            global.App.sipsRules = magicObj.sipsList;
            global.App.magicSet = magicObj.magicSet;
            global.App.log = datalog.log;
        }

        var rules = [];
        var totalRules = 0;

        if (selectedRowsId) {

            global.App.magicSetStr = "";
            var queryRules = [];
            var answerRules = [];

            for (var num in selectedRowsId) {
                var index = selectedRowsId[num] - 1;
                //console.log("num:" + index);
                var magicRule = global.App.magicSet[index];
                //console.log("num:" + index + " rule:" + magicRule);

                for (var i = 0; i < magicRule.queryRules.length; i++) {

                    queryRules.push({
                        id: totalRules++,
                        rule: magicRule.queryRules[i].toString()
                    });
                    global.App.magicSetStr += magicRule.queryRules[i].toString() + ".\n";
                }

                for (var i = 0; i < magicRule.answerRules.length; i++) {

                    answerRules.push({
                        id: totalRules++,
                        rule: magicRule.answerRules[i].toString()
                    });
                    global.App.magicSetStr += magicRule.answerRules[i].toString() + ".\n";
                }
            }

            rules = queryRules.concat(answerRules);

        } else {

            for (var i = 0; i < global.App.queryRules.length; i++) {
                totalRules = i;
                rules.push({
                    id: i + 1,
                    rule: global.App.queryRules[i].toString()
                });
            }

            for (var i = 0; i < global.App.answerRules.length; i++) {
                totalRules += 1;
                rules.push({
                    id: totalRules + 1,
                    rule: global.App.answerRules[i].toString()
                });
            }
        }

        //console.log("Magic Rules:" + JSON.stringify(rules));
        var RuleTokenList = datalog.lexer(global.App.magicSetStr);

        var parserRuleObj = new datalog.Parser(RuleTokenList);

        parserRuleObj.parseRules();

        if (!parserRuleObj.errorParser) {

            parserRuleObj.dg.analyze();

            global.App.msnodes = parserRuleObj.dg.nodes;
            global.App.msedges = parserRuleObj.dg.edges;

        }

        callback(null, {
            randomNumber: 20,
            data: rules,
            params: params
        });

    },

    queryDb: function(params, callback) {

        datalog.log = [];

        var rules = global.App.loadedRules;
        var facts = global.App.loadedFacts;
        dbase = new datalog.Database(facts.concat(rules));

        var goalToken = datalog.lexer(params.query_string);
        var goalParserObj = new datalog.Parser(goalToken);
        var goal = goalParserObj.parseQuery();
        var success = false;
        var answers = [];

        if (!goalParserObj.errorParser) {

            var result = false;
            var i = 0;

            console.log('Executing queryDb with goal:' + goal);

            for (var item of dbase.queryDB(goal)) {
                //console.log('item:' + item);
                answers.push({
                    id: i + 1,
                    result: item
                });
                i++;
                result = true;
            }

            if (!result) {
                answers.push({
                    id: 1,
                    result: ' No solutions'
                });
            }
            success = true;
        } else {
            answers.push({
                id: 1,
                result: '<div class="alert alert-danger"><strong> Parsing query Error!</strong> </div>'
            });
            console.log('Parsing query Error!');
            datalog.log.push('<div class="alert alert-danger"><strong> Parsing query Error!</strong> </div>');

        }

        global.App.log = datalog.log;

        callback(null, {
            success: success,
            data: answers,
            params: params
        });

    }
};

module.exports = Interpreter;