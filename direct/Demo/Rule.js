var table = 'RULE';

var db = global.App.database.connection;

var Rule = {
    create: function(params, callback) {

        console.log(params);
        delete params['id'];
        db.query('INSERT INTO ' + table + ' SET ?', params, function(err, result) {

            if (err) {
                db.debug(err, callback);
                return false;
            }

            db.query('SELECT * FROM ' + table + ' WHERE id = ?', result.insertId, function(err, rows, fields) {
                callback(null, {
                    data: rows[0]
                });
            });
        });
    },

    //callback as last argument is mandatory
    read: function(params, callback) {

        var sql = 'SELECT @curRow := @curRow + 1 AS num, id, value FROM ' + table + ' JOIN(SELECT @curRow := 0) r ';
        where += ' WHERE id_db=' + db.escape(params.db_id);

        if (params.relation) {
            //console.log(params['relation']);
            where += ' AND relation="' + params['relation'] + '"';
        }

        //filtering. this example assumes filtering on 1 field, as multiple field where clause requires additional info e.g. chain operator
        if (params.filter) {
            where += " AND `" + params.filter[0].property + "` LIKE '%" + params.filter[0].value + "%'"; // set your business logic here to perform advanced where clause
            //sql += where;
        }

        // this sample implementation supports 1 sorter, to have more than one, you have to loop and alter query
        if (params.sort) {
            var s = params.sort[0];
            where += ' ORDER BY ' + db.escape(s.property) + ' ' + db.escape(s.direction);
        }

        // Paging
        sql += where + ' LIMIT ' + db.escape(params.start) + ' , ' + db.escape(params.limit);

        db.query(sql, function(err, rows, fields) {
            if (err) {
                db.debug(err, callback);
                return false;
            }

            //get totals for paging

            var totalQuery = 'SELECT count(*) as totals from ' + table + where;

            db.query(totalQuery, function(err, rowsTotal, fields) {
                if (err) {
                    db.debug(err, callback);
                    return false;
                }

                callback(null, {
                    data: rows,
                    total: rowsTotal[0].totals
                });
            });
        });

    },

    update: function(params, callback) {
        db.query('UPDATE ' + table + ' SET ? where id = ' + db.escape(params['id']), params, function(err, result) {
            if (err) {
                db.debug(err, callback);
                return false;
            }

            callback();
        });
    },

    destroy: function(params, callback) {
        db.query('DELETE FROM ' + table + ' WHERE id = ?', db.escape(params['id']), function(err, rows, fields) {
            if (err) {
                db.debug(err, callback);
                return false;
            }

            callback(null, {
                success: rows.affectedRows === 1, //if row successfully removed, affected row will be equal to 1
                id: params['id']
            });
        });
    }
};

module.exports = Rule;
