var db = global.App.database.connection;

var Tree = {

    getTree: function(params, callback) {

        var out = [],
            num, i;

        var pref = params.node.substring(0, 3);

        //var sql = 'SELECT * FROM ' + table,
        //    where = '';

        var sql = ' select id,name as db_name from db';

        if (pref == "fa_") {
            console.log("1pref:" + pref);
            var db_id = params.node.substring(6, params.node.length);
            console.log("1db_id:" + db_id + " params.node:" + params.node);
            sql = ' select distinct relation as fact_relation, name as db_name, db.id as db_id from db, fact  WHERE db.id = fact.id_db AND db.id= ' + db_id;

        } else

        if (pref == "ru_") {
            console.log("pref:" + pref);
            var db_id = params.node.substring(6, params.node.length);
            console.log("db_id:" + db_id + " params.node:" + params.node);
            sql = ' select distinct relation as rule_relation, name as db_name,  db.id as db_id from db, rule  WHERE db.id = rule.id_db AND db.id= ' + db_id;

        }

        ////filtering. this example assumes filtering on 1 field, as multiple field where clause requires additional info e.g. chain operator

        //if (params.filter) {
        //    where = " WHERE `" + params.filter[0].property + "` LIKE '%" + params.filter[0].value + "%'"; // set your business logic here to perform advanced where clause
        //    sql += where;
        //}

        //// this sample implementation supports 1 sorter, to have more than one, you have to loop and alter query
        //if (params.sort) {
        //    var s = params.sort[0];
        //    sql = sql + ' ORDER BY ' + db.escape(s.property) + ' ' + db.escape(s.direction);
        //}

        //// Paging
        //sql = sql + ' LIMIT ' + db.escape(params.start) + ' , ' + db.escape(params.limit);

        db.query(sql, function(err, rows, fields) {
            if (err) {
                db.debug(err, callback);
                console.log("err:" + err);
                return false;
            }

            //get totals for paging

            //var totalQuery = 'SELECT count(*) as totals from db';

            //db.query(totalQuery, function (err, rowsTotal, fields) {
            //    if (err) {
            //        db.debug(err, callback);
            //        return false;
            //    }

            //generating dummy dynamic tree

            console.log("params:" + JSON.stringify(params) + " pref:" + pref);
            if (params.node === "root") {
                for (i = 0; i < rows.length; ++i) {
                    out.push({
                        id: "db_" + rows[i].id,
                        db_id: rows[i].id,
                        text: rows[i].db_name,
                        //currentTest: 2,
                        //testStatus: 1,
                        //totalTests: 10,
                        //ignoredTests: 1,
                        type: 'database',
                        leaf: false,
                        glyph: 'xf0ae@FontAwesome'

                    });
                }
            } else if (pref == "db_") {
                //var db_id= = params.node.substr(0, 1);

                var db_id = params.node.replace("db_", "");
                // for(i = 1; i <= 2; ++i) {
                out.push({
                    id: 'fa_' + params.node,
                    db_id: db_id,
                    text: 'Facts',
                    type: 'fact',
                    leaf: false,
                    //iconCls: 'fa fa-table'
                    glyph: 'xf0ce@FontAwesome'
                }, {
                    id: 'ru_' + params.node,
                    text: 'Rules',
                    db_id: db_id,
                    type: 'rule',
                    leaf: false,
                    // iconCls: 'glyphicon glyphicon-indent-left'
                    glyph: 'xe057@Glyphicons Halflings'
                });

                callback(null, out);
                // db.end();
            } else if (pref == "fa_") {
                //num = params.node.substr(0, 1);
                for (i = 0; i < rows.length; ++i) {
                    out.push({
                        id: params.node + i,
                        text: rows[i].fact_relation,
                        db_id: rows[i].db_id,
                        leaf: true
                    });
                }
            } else if (pref == "ru_") {
                //num = params.node.substr(0, 1);
                for (i = 0; i < rows.length; i++) {
                    out.push({
                        id: params.node + i,
                        text: rows[i].rule_relation,
                        db_id: rows[i].db_id,
                        leaf: true
                    });
                }
            }

            callback(null, out);
            //db.end();
            //   });

        });

    }
};

module.exports = Tree;
