-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for extdirectnode
-- DROP DATABASE IF EXISTS `extdirectnode`;
CREATE DATABASE IF NOT EXISTS `extdirectnode` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `extdirectnode`;


-- Dumping structure for table extdirectnode.db
DROP TABLE IF EXISTS `db`;
CREATE TABLE IF NOT EXISTS `db` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1679 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table extdirectnode.db: 2 rows
/*!40000 ALTER TABLE `db` DISABLE KEYS */;
INSERT INTO `db` (`id`, `name`) VALUES
	(1, 'Database-simple'),
	(2, 'Database-not'),
	(3, 'Database-error'),
	(4, 'Database-sips'),
	(5, 'Database-magicsets'),
	(6, 'Database-family'),
	(7, 'Database-rec'),
	(8, 'Database-path-graph');
/*!40000 ALTER TABLE `db` ENABLE KEYS */;


-- Dumping structure for table extdirectnode.fact
DROP TABLE IF EXISTS `fact`;
CREATE TABLE IF NOT EXISTS `fact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_db` int(11) NOT NULL,
  `relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `json_object` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1679 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table extdirectnode.fact: 11 rows
/*!40000 ALTER TABLE `fact` DISABLE KEYS */;
INSERT INTO `fact` (`id`, `id_db`, `relation`, `value`, `json_object`) VALUES
	(1, 1, 's', 's(2,1).', NULL),
	(2, 1, 's', 's(3,4).', NULL),
	(3, 1, 's', 's(2,2).', NULL),
	(4, 1, 's', 's(5,2).', NULL),
	(5, 1, 's', 's(2,3).', NULL),
	(6, 1, 't', 't(5).', NULL),
	(7, 1, 't', 't(6).', NULL),
	(8, 1, 't', 't(7).', NULL),
	(9, 1, 'r', 'r(1).', NULL),
	(10, 1, 'r', 'r(3).', NULL),
	(11, 1, 'r', 'r(4).', NULL),
	(12, 2, 's', 's(2,1).', NULL),
	(13, 2, 's', 's(3,4).', NULL),
	(14, 2, 's', 's(2,2).', NULL),
	(15,2 , 's', 's(5,2).', NULL),
	(16,2 , 'r', 'r(1).', NULL),
	(17,2 , 'r', 'r(3).', NULL),
	(18,2 , 'r', 'r(5).', NULL),
	(19,2 , 't1', 't1(5).', NULL),
	(20,2 , 't1', 't1(6).', NULL),
	(21,2 , 't2', 't2(4).', NULL),
	(22,3 , 'e', 'e(2,1)..', NULL),
	(23,3 , 'e', ' e(3,5).', NULL),
	(24,4 , 't', ' t(1,2).', NULL),
	(25,4 , 'r', '  r(2,4).', NULL),
	(26,4 , 'c', ' c(3,5).', NULL),
	(27,6 , 'c', ' c(charles, elizabeth, philip).', NULL),
	(28,6 , 'c', ' c(anne, elizabeth, philip).', NULL),
	(29,6 , 'c', ' c(andrew, elizabeth, philip).', NULL),
	(30,6 , 'c', ' c(edward, elizabeth, philip).', NULL),
	(31,6 , 'c', ' c(william, diana, charles).', NULL),
	(32,6 , 'c', ' c(harry, diana, charles).', NULL),
	(33,6 , 'c', ' c(peter, anne, mark).', NULL),
	(34,6 , 'c', ' c(zara, anne, mark).', NULL),
	(35,6 , 'c', ' c(beatrice, sarah, andrew).', NULL),
	(36,6 , 'c', ' c(eugenie, sarah, andrew).', NULL),
	(37,6 , 'c', ' c(louise, sophie, edward).', NULL),
	(38,6 , 'c', ' c(james, sophie, edward).', NULL),
	(39,6 , 'c', ' c(savannah, autumn, peter).', NULL),
	(40,6 , 'c', ' c(george, kate, william).', NULL),
	(41,6 , 'c', ' c(charlotte, kate, william).', NULL),
	(42,6 , 'c', ' c(isla, autumn, peter).', NULL),
	(43,6 , 'c', ' c(grace, zara, mike).', NULL),
	(44,8 , 'edge', ' edge(a,b).', NULL),
	(45,8 , 'edge', ' edge(b,c).', NULL),
	(46,8 , 'edge', ' edge(c,d).', NULL);
	-- (40,7 , 'b2', ' b2(1).', NULL),
	-- (41,7 , 'b2', ' b2(3).', NULL),
	-- (42,7 , 'b2', ' b2(6).', NULL),
	-- (43,7 , 'b2', ' b2(7).', NULL),
	-- (44,7 , 'b1', ' b1(1,1).', NULL),
	-- (45,7 , 'b1', ' b1(5,4).', NULL),
	-- (46,7 , 'b1', ' b1(2,2).', NULL),
	-- (47,7 , 'b1', ' b1(6,1).', NULL),
	-- (48,7 , 'b1', ' b1(3,5).', NULL),
	-- (49,7 , 'b1', ' b1(7,4).', NULL),
	-- (50,7 , 'b1', ' b1(4,3).', NULL);


       
/*!40000 ALTER TABLE `fact` ENABLE KEYS */;


-- Dumping structure for table extdirectnode.rule
DROP TABLE IF EXISTS `rule`;
CREATE TABLE IF NOT EXISTS `rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_db` int(11) NOT NULL,
  `relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `json_object` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1679 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table extdirectnode.rule: 4 rows
/*!40000 ALTER TABLE `rule` DISABLE KEYS */;
INSERT INTO `rule` (`id`, `id_db`, `relation`, `value`, `json_object`) VALUES
	(1, 1, 'n', 'n(X):-s(Y,X),q(Y).', NULL),
	(2, 1, 'q', 'q(Y):-r(Y).', NULL),
	(3, 1, 'q', 'q(Y):-t(Y).', NULL),
	(4, 1, 'p', 'p(X,Y):-s(X,Y), not q(Y).', NULL),
	(5, 1, 'm', 'm(X) :-r(X).', NULL),
	(6, 1, 'm', 'm(X) :-s(X,Y), m(Y).', NULL),
	(7, 2, 'q', 'q(Y):-t2(Y).', NULL),
	(8, 2, 'r', 'r(Y):-t1(Y).', NULL),
	(9, 2, 'p', 'p(X,Y):-s(X,Y), not r(Y),q(Y).', NULL),
	(10, 3, 'p', 'p(X,Y):-e(Y,X).', NULL),
	(11, 3, 'p', 'p(X,Y):-e(X,Z), p(Z,Y).', NULL),
	(12, 4, 'p', 'p(X,Y,Z):-q(Z,W),r(X,Z),s(W,Y,X).', NULL),
	(13, 4, 'q', ' q(Z,W):-t(Z,W).', NULL),
	(14, 4, 's', ' s(W,Y,X):-c(X,Y),q(X,W).', NULL),
	(15, 5, 'p', ' p(X) :-s(X,Y), not r(Y), p(Y).', NULL),
	(16, 5, 'p', ' p(X) :-q(X).', NULL),
	(17, 5, 'r', ' r(Y) :-t1(X,Y).', NULL),
	(18, 5, 'q', ' q(X) :-t2(X,Y).', NULL),
	(19, 6, 'p', ' p(X, Y) :- c(Y, _, X).', NULL),
	(20, 6, 'p', ' p(X, Y) :- c(Y, X, _).', NULL),
	(21, 6, 'sg', ' sg(X, Y) :- p(Z, X), p(Z, Y).', NULL),
	(22, 6, 'sg', ' sg(X, Y) :- p(Z1, X), sg(Z1, Z2), p(Z2, Y).', NULL),
	(23, 7, 'p', ' p(X) :- b1(X,Y), p(Y).', NULL),
	(24, 7, 'p', ' p(X) :- b1(X,X), b2(X).', NULL),
	(25, 7, 'r', ' r(X) :- p(X).', NULL),
	(26, 7, 'r', ' r(X) :- b1(Z,X), not p(Z).', NULL),
	(27, 7, 't', ' t(X) :- r(X), q(X,Y), r(Y).', NULL),
	(28, 7, 'q', ' q(X,Y) :- b2(X), b2(Y).', NULL),
	(29, 7, 's', ' s(X) :- q(X,Y), r(Y).', NULL),
	(30, 7, 'u', ' u(X,X) :- s(X), s(Y).', NULL),
	(31, 7, 'v', ' v(X) :- not u(X,X), t(X).', NULL),
	(32, 8, 'path', ' path(X,Y) :- edge(X,Y).', NULL),
	(33, 8, 'path', ' path(X,Y) :-  edge(X,Z),path(Z,Y).', NULL);

       

/*!40000 ALTER TABLE `rule` ENABLE KEYS */;

