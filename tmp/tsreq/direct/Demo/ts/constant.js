/// <reference path="term4.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var datalog;
(function (datalog) {
    var Constant = (function (_super) {
        __extends(Constant, _super);
        function Constant(value) {
            _super.call(this, "Constant");
            this.toString = function () {
                return this.value;
            };
            this.value = value;
            this.isConstant = true;
        }
        return Constant;
    }(datalog.Term));
})(datalog || (datalog = {}));
