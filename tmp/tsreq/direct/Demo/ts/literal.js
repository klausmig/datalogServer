/// <reference path="term4.ts"/>
/// <reference path='variable.ts'/>
/// <reference path='constant.ts'/>
var datalog;
(function (datalog) {
    var Literal = (function () {
        function Literal(relation, listTerm, isPositive) {
            this.isPositive = true;
            this.isGround = false;
            this.substitute = function (bindings) {
                var argSubstitute = this.listTerm.map(function (term) {
                    if (term instanceof Variable) {
                        console.log('Termtosubstitute:' + term + ' bindings:' + JSON.stringify(bindings.slice()));
                        return term.substitute(bindings);
                    }
                    else
                        return term; //It is a Constant
                });
                var newLiteralSustutute = new Literal(this.relation, argSubstitute, this.isPositive);
                return newLiteralSustutute;
            };
            this.query = function (database) {
                yield* database.query(this);
            };
            this.toString = function () {
                if (this.listTerm.length === 0) {
                    return this.relation;
                }
                if (this.isPositive)
                    return this.relation + '(' + this.listTerm.join(',') + ')';
                else
                    return " not " + this.relation + '(' + this.listTerm.join(',') + ')';
                //if (this.totalBind)
                //    return this.relation + '(' + this.listTerm.join(',') + ')' + this.totalBind;
                //else
            };
            this.relation = relation;
            this.listTerm = listTerm || [];
            this.isPositive = isPositive;
        }
        Literal.prototype.isFactLiteral = function () {
            for (var _i = 0, _a = this.listTerm; _i < _a.length; _i++) {
                var term = _a[_i];
                //console.log("validation:" + term + " type:" + term.type);
                if (term.type == "Variable") {
                    this.isGround = false;
                    return false;
                }
            }
            this.isGround = true;
            return true;
        };
        Literal.prototype.match = function (otherLiteral) {
            var matchObj = new Match();
            if (otherLiteral instanceof Literal) {
                if ((this.relation !== otherLiteral.relation) || (this.listTerm.length !== otherLiteral.listTerm.length)) {
                    return null;
                }
                //console.log("actualliteral:" + this);
                //if (this.isGround) {
                console.log("Actualliteral:" + this + " otherLiteral:" + otherLiteral);
                for (var i = 0; i < otherLiteral.listTerm.length; i++) {
                    var term = this.listTerm[i];
                    var otherTerm = otherLiteral.listTerm[i];
                    //console.log('bindings:' + JSON.stringify([...matchObj.bindingMap]));
                    if (!term.isConstant && otherTerm.isConstant) {
                        matchObj.bindingMap.set(term, otherTerm); // binding variable and constant
                    }
                    else if (term.isConstant && !otherTerm.isConstant) {
                        matchObj.bindingMap.set(otherTerm, term); // binding variable and constant
                    }
                    else if (term.isConstant && otherTerm.isConstant) {
                        if (term.value !== otherTerm.value) {
                            console.log("diff headterm:" + this.listTerm[i] + " goalterm:" + otherLiteral.listTerm[i]);
                            return null;
                        }
                        else {
                            matchObj.bindingMap.set(term, otherTerm);
                        }
                    }
                }
                console.log('Bindings:' + JSON.stringify(matchObj.bindingMap.slice()));
                return matchObj.bindingMap;
            }
        };
        ;
        return Literal;
    }());
    datalog.Literal = Literal;
})(datalog || (datalog = {}));
