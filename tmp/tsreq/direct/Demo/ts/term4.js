var datalog;
(function (datalog) {
    var Term = (function () {
        function Term(type) {
            this.isConstant = false;
            this.toString = function () {
                return this.type;
            };
            this.type = type;
        }
        return Term;
    }());
    datalog.Term = Term;
})(datalog || (datalog = {}));
