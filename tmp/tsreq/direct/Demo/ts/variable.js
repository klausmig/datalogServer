/// <reference path="term4.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var datalog;
(function (datalog) {
    var Variable = (function (_super) {
        __extends(Variable, _super);
        function Variable(name) {
            _super.call(this, 'Variable');
            this.match = function (other) {
                var bindings = new Map;
                if (this !== other) {
                    bindings.set(this, other);
                }
                return bindings;
            };
            this.substitute = function (bindings) {
                var value = bindings.get(this);
                if (value) {
                    // if value is a compound term then substitute
                    // variables inside it too
                    return value;
                }
                return this;
            };
            this.toString = function () {
                return this.name;
            };
            this.name = name;
        }
        return Variable;
    }(datalog.Term));
    datalog.Variable = Variable;
})(datalog || (datalog = {}));
