"use strict";
///<reference path='node.d.ts'/>
var Term = (function () {
    function Term(type) {
        this.isConstant = false;
        this.toString = function () {
            return this.type;
        };
        this.type = type;
    }
    return Term;
}());
exports.Term = Term;
