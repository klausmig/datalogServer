"use strict";

class DirectGraphController {

    directGraph: any;
    nodes: any [];
    edges: any;


    constructor(directGraph) {
       
        this.directGraph = directGraph;

    }

    analyze = function() {

        for (let pair of this.directGraph) {
            console.log(pair[0] + "[" + pair[1].join(',') + "]");
            this.nodes.unshift({ group: "nodes", data: { id: pair[0] } });
            for (let edgeNode of pair[1]) {
                this.edges.unshift({ group: "edges", data: { id:  edgeNode +"sd"+ pair[0], source: edgeNode , target: pair[0] } });
                
            }
        
        }

     
    }


   toString = function() {
        return this.name;
    };
}
