

function *lexer(text) {
	
    var tokenRegexp = /[A-Za-z_\d]+|:\-|[()\.,~]/g;
    var match;
    while ((match = tokenRegexp.exec(text)) !== null) {
    	//console.log("match:"+match[0]);
        yield match[0];
    }
}
