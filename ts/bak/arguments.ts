"use strict";
class Arguments {
    listTerm;

    constructor(listTerm) {
        this.listTerm = listTerm;
    }

    query = function*(database) {
        var self = this;
        function* solutions(index, bindings) {
            var arg = self.listTerm[index];
            if (!arg) {
                yield self.substitute(bindings);
            } else {
                for (var item of database.query(arg.substitute(bindings))) {
                    var unified = mergeBindings(arg.match(item), bindings);
                    if (unified) {
                        yield* solutions(index + 1, unified);
                    }
                }
            }
        }
        yield* solutions(0, new Map);
    
    };

    substitute(bindings) {
        return new Arguments(this.listTerm.map(function(arg) {
            return arg.substitute(bindings);
        }));
    }

    toString()  {
        return this.listTerm.join(',');
    }
}