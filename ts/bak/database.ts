"use strict";
class Database {
    rules: Rule [];

    constructor(rules) {
        this.rules = rules;
    }

    query = function* (goal) {
        for (var i = 0, rule; rule = this.rules[i]; i++) {

            var match = rule.head.match(goal);

            if (match) {
        	
                //console.log("rulein:"+rule);
                console.log("head:" + rule.head + " goal:" + goal + " match:" + JSON.stringify([...match]));
                //
                var head = rule.head.substitute(match);

                console.log("head-subs:" + head);

                if (!rule.isFact) {

                    var body = rule.substituteBody(match);

                    console.log("body-subs:" + body);
            
                    //console.log("rulesust:"+rule);

                    var newQueryRule = new Rule(head,body);
           
                    //throw new Error("Debug");
                    for (var item of newQueryRule.queryBody(this)) {
                        console.log("item:" + item + " map:" + JSON.stringify( [...head.match(item)]));
                       // yield head.substitute(head.match(item));
                        yield item;
                        //  throw new Error("Debug");
                    }
                }
                else
                    yield head;
            }
        }
    };

    toString() {
        return this.rules.join('.\n') + '.';
    }


    getRules() {
        return this.rules;
    }
}
