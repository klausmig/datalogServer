

//testing git

declare var cy: any;

function mergeBindings(bindings1, bindings2) {
    if (!bindings1 || !bindings2) {
        return null;
    }
    var conflict = false;
    var bindings = new Map;
    
   // console.log('bindings1:'+JSON.stringify([...bindings1])+ ' bindings2:'+JSON.stringify([...bindings2]));
    		
    bindings1.forEach(function(value, variable) {
        bindings.set(variable, value);
    });
    
    //console.log('bindings1map:'+JSON.stringify([...bindings]));
    bindings2.forEach(function(value, variable) {
    	
    	// console.log('value:'+value+" variable"+variable);
    	 
    	 
        var other = bindings.get(variable);
        if (other) {
            var sub = other.match(value);
            if (!sub) {
                conflict = true;
            } else {
                sub.forEach(function(value, variable) {
                    bindings.set(variable, value);
                });
            }
        } else {
            bindings.set(variable, value);
        }
    });
    if (conflict) {
        return null;
    }
    //console.log('bindingsfinal:'+JSON.stringify([...bindings]));
    return bindings;
};



window.onload = () => {

/*
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();*/

    document.getElementById('submit').addEventListener('click', function () {
       // console.log('rules:' + (<HTMLInputElement>document.getElementById('rules')).value);
       // var rules = parser(lexer((<HTMLInputElement>document.getElementById('rules')).value)).parseRules();

        var FactTokenList = lexer($("#facts").val());
        var RuleTokenList = lexer($("#rules").val());


        var parserFactObj = new Parser(FactTokenList);
        var parserRuleObj = new Parser(RuleTokenList);

        var facts = parserFactObj.parseRules();
        var rules = parserRuleObj.parseRules();

        var db = new Database(facts.concat(rules));

        console.log("DATABASE:\n" + db);

        var goalToken = lexer($("#query").val());

        var goalParserObj = new Parser(goalToken);

        var goal = goalParserObj.parseQuery();
        //var goal = "";
        //console.log( " tokenList:" + tokenList + " id:" + $("#rules").val());
        //console.log("parsed goal:" + goal + " goalToken:" + goalToken + " id:" + $("#query").val());
        var list = document.getElementById('answers');
        list.innerHTML = '';
   
        for (var item of db.query(goal)) {
            var li = document.createElement('LI');
            li.textContent = item;
            list.appendChild(li);
        } 

        if (list.innerHTML === '') {
            list.innerHTML = 'No solutions';
        }
    });

    $("#loadDB").click(function () {
    //document.getElementById('loadDB').addEventListener('click', function () {
       // console.log('rules:' + (<HTMLInputElement>document.getElementById('rules')).value);
        //console.log('rules:' + $("#rules").val());
        var FactTokenList = lexer($("#facts").val() );
        var RuleTokenList = lexer($("#rules").val());


        var parserFactObj = new Parser(FactTokenList);
        var parserRuleObj = new Parser(RuleTokenList);

        var facts = parserFactObj.parseRules();
        var rules = parserRuleObj.parseRules();

        

        //console.log(rules);

       // console.log("directgraph:" +parserObj.getDirectGraph());

        parserRuleObj.analyze();
        parserFactObj.analyze();
        var db = new Database(rules);

        //var magicObj = new MagicSet(db, parserRuleObj.directGraph);
        //magicObj.generateMS();
        //console.log("DATABASE:\n" + magicObj);

        //$("#magicsets").val("DATABASE:\n" + magicObj);



       // var graphControllerObj = new DirectGraphController(parserObj.directGraph);

       // graphController.analyze();
       /*
        cy.add({
       
            group: "nodes",
            data: { id:'test', weight: 75 },
            position: { x: 200, y: 200 }
        }); */

        //console.log(graphController.nodes);
        var allNodes = parserRuleObj.nodes.concat(parserFactObj.nodes);
        //console.log(allNodes);
        cy.add(allNodes);
        cy.add(parserRuleObj.edges);
        //console.log(JSON.stringify(parserRuleObj.nodes));
        //console.log(JSON.stringify(parserRuleObj.edges));

        cy.layout({
           // name: 'breadthfirst',
            name: 'dagre',
            directed: true,
            padding: 10,
            roots: '#r',
        });

        var bfs = cy.elements().bfs('#r', function () { }, true);

        var i = 0;
        var highlightNextEle = function () {
            if (i < bfs.path.length) {
                bfs.path[i].addClass('highlighted');

                i++;
                setTimeout(highlightNextEle, 1000);
            }
        };

        // kick off first highlight
        highlightNextEle();

        console.log("DATABASE:\n" + db);

       
    });

};


