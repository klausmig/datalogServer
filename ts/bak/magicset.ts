"use strict";
class MagicSet{

    ruleDb:Database;
    directGraph;
    answerList: Rule[];
    queryList: Rule[];

    constructor(ruleDb, directGraph) {
        this.ruleDb = ruleDb;
        this.directGraph = directGraph;
    }


    getCombinations (chars):any {
    var result = [];
    var f = function (prefix, chars) {
        for (var i = 0; i < chars.length; i++) {
            result.push(prefix + chars[i]);
            f(prefix + chars[i], chars.slice(i + 1));
        }
    }
    f('', chars);
    return result;
}
/*
Usage:

var combinations = getCombinations(["a", "b", "c", "d"]);
Result:

["a", "ab", "abc", "abcd", "abd", "ac", "acd", "ad", "b", "bc", "bcd", "bd", "c", "cd", "d"] */


    generateMS(): any {

       this.answerList= [];
       this.queryList = [];

       for (let rule of this.ruleDb.getRules()) {

           

            var listAdormentStr = "";

            for (var i = 1; i <= rule.head.listTerm.length; i++) {
                listAdormentStr += '' + i;
            }
            //console.log("adorment:" + listAdormentStr);

            //if (listAdormentStr.length > 1) {

            var listAdorment = this.getCombinations(listAdormentStr)
            listAdorment.push('0');

            console.log("adormentCombinations:" + listAdorment);

            for (let adorment of listAdorment) {
                //console.log("1processing rule:" + rule);

                var unifiedVar = new Map(); 

                var sipsBody;

                var sipsObj = new SIPS(rule, adorment);

                if (adorment != '0') {
                    
                    sipsBody = sipsObj.optimalRule();
                }
                else
                    sipsBody = sipsObj.optimalRuleWithoutAdorment(this.directGraph);
                 
                var listTermQuery = [];

                //console.log("terms:" + rule.head.listTerm + " adorment:" + adorment);

                if (adorment != '0') 
                    for (var i = 0; i < adorment.length; i++) {
                        
                        var term = <Variable>rule.head.listTerm[(<number>adorment[i]) - 1];
                        listTermQuery.push(term);
                        unifiedVar.set(term.name, true);
                        //console.log("terms:" + rule.head.listTerm + " adorment:" + adorment);
                    }
               //console.log("listTermQuery:" + listTermQuery);

               var answerHead = new AnswerLiteral(rule.head.relation, rule.head.listTerm, rule.head.isPositive, adorment);
               var querySeed = new QueryLiteral(rule.head.relation, listTermQuery, adorment);

                var listLiteral = [];

                listLiteral.push(querySeed);

                for (let bodyliteral of sipsBody) {

                    var newLiteral;
                    var queryLiteral;

                    //console.log("  sipsBody:" + sipsBody + " bodyliteral:" + bodyliteral );

                    if (this.directGraph.has(bodyliteral.relation)) { //It is a derivated relation

                        var listTermQueryLiteral = []
                        var adormentQuery = "";

                       // console.log("unifiedVarmap:" + JSON.stringify([...unifiedVar]));

                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {
                        
                            var term = <Variable>bodyliteral.listTerm[i];
                       
                            if (unifiedVar.has(term.name)) {
                                listTermQueryLiteral.push(term);
                                adormentQuery += "" + (i + 1);

                               // console.log("  bodyliteral.listTerm:" + bodyliteral.listTerm + " term:" + term + " adormentQuery:" + adormentQuery);
                            }
                        }

                        newLiteral = new AnswerLiteral(bodyliteral.relation, bodyliteral.listTerm, bodyliteral.isPositive, adormentQuery);
                        queryLiteral = new QueryLiteral(bodyliteral.relation, listTermQueryLiteral, adormentQuery);

                        console.log("  AnswerLiteral:" + newLiteral + "  Positive:" + newLiteral.isPositive + " queryLiteral:" + queryLiteral);

                    
                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {
                           
                            var term = <Variable>bodyliteral.listTerm[i];
                            //console.log("aca- terms:" + bodyliteral.listTerm + " term.name added:" + term.name + " adorment:" + adorment);
                            unifiedVar.set(term.name, true);
                        }

                        var newlistLiteral = listLiteral.slice();

                        var MSQueryRule = new Rule(queryLiteral, newlistLiteral);

                        //if (!adorment0Added) {
                        //    var querySeed0 = new QueryLiteral(rule.head.relation, [], '0');

                        //}

                        this.queryList.push(MSQueryRule);

                        console.log("MSQueryRule:" + MSQueryRule);


                    }
                    else {  //base relation
                        newLiteral = bodyliteral;
                       
                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {
                           
                            var term = <Variable>bodyliteral.listTerm[i];
                            //console.log("aca2-terms:" + bodyliteral.listTerm + " term.name added:" + term.name + " adorment:" + adorment);
                            unifiedVar.set(term.name, true);
                        }
                        

                    }
                    listLiteral.push(newLiteral);
                }

                var MSAnswerRule = new Rule(answerHead, listLiteral);
                this.answerList.push(MSAnswerRule);
                console.log("MSRule:" + MSAnswerRule);

            }
                

    }


    }

    toString = function () {
    
        return "MAGIC RULES:\n\n" + this.queryList.join('.\n') + '.' + '\n\n' + this.answerList.join('.\n') + '.';
    };
}

