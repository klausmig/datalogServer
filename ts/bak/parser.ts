"use strict";
class Parser {

    tokens: any;
    currentToken: any;
    doneTokenList: any;
    variableScope;
    directGraph;
    isDerivatedRelation;
    nodes:any;
    edges: any;

    constructor(tokens) {
        this.tokens = tokens;
        this.directGraph = new Map();
        this.isDerivatedRelation = new Map();
        this.nodes = [];
        this.edges = [];
        //console.log("cont tokens:" + this.tokens);
        
    }

    nextToken():void {
        var nextTokenList = this.tokens.next();
        this.currentToken = nextTokenList.value;
        this.doneTokenList = nextTokenList.done;
       
    }
    getDirectGraph() {

        var mapStr:string = "";
   
        for (let pair of this.directGraph) {
            //console.log(pair[0] + "[" + pair[1].join(',') + "]");
            mapStr += pair[0] + "[" + pair[1].join(',') + "]";
        }

        return mapStr;
    }

    analyze  () {

        for (let pair of this.directGraph) {
            //console.log(pair[0] + "[" + pair[1].join(',') + "]");
            this.nodes.unshift({ group: "nodes", data: { id: pair[0] } });
            for (let edgeNode of pair[1]) {
                this.edges.unshift({ group: "edges", data: { id: pair[0] + edgeNode  , source: pair[0] , target:  edgeNode } });
            } 

        }
    }
    
    parseAtom():string {
        var name = this.currentToken;
        if (!/^[A-Za-z_\d]+$/.test(name)) {
            throw new SyntaxError('Bad atom name: ' + name);
        }
        this.nextToken();
        return name;
    }


    parseTerm():any {

        var name = this.parseAtom();
        
        if (/^[A-Z_][A-Za-z_\d]*$/.test(name)) {
            if (name === '_') {
                return new Variable('_');
            }
            // variable X in the same scope should point to the same object
            var variable = this.variableScope[name];
            if (!variable) {
                variable = this.variableScope[name] = new Variable(name);
            }
            return variable;
        }
       
        return new Constant(name);
        
    }
    
    parseLiteral(): Literal {

        //console.log("Parsing Literal ...");
        //console.log("this.currentToken:" + this.currentToken);

        var isPositiveLiteral = true;
        if ((this.currentToken === 'not') || (this.currentToken === '~')) {

            this.nextToken();
            isPositiveLiteral = false;
        }
        //console.log("this.currentToken:" + this.currentToken);
        var name = this.parseAtom();
       // var arguments: Arguments;

        if (this.currentToken === '(') {
            //console.log("this.currentToken:" + this.currentToken);
            this.nextToken(); // eat (
            //console.log("this.currentToken:" + this.currentToken);
            var listTerm = [];
            while (this.currentToken !== ')') {
                listTerm.push(this.parseTerm());
                if (this.currentToken !== ',' && this.currentToken !== ')') {
                    throw new SyntaxError('Expected , or ) in Literal but got ' + this.currentToken);
                }
                if (this.currentToken === ',') {
                    this.nextToken(); // eat ,
                }
               //console.log("this.currentToken:" + this.currentToken);
            }
            this.nextToken(); // eat )
           // arguments = new Arguments(listTerm);
            //console.log("this.currentToken:" + this.currentToken);
        }

        //console.log("literal:" + name + listTerm + isPositiveLiteral);
   
        return new Literal(name, listTerm, isPositiveLiteral);

    }
    
    
    
     parseRule():any {
        var head = this.parseLiteral();

        //console.log('head:'+head);

        if (this.currentToken === '.') { // Is a Fact with body TRUE
            this.nextToken(); // eat .
           // return new Rule(head, Literal.TRUE);
            if (head.isFactLiteral()){
                var newFact = new Rule(head, []);
                newFact.isFact = true;
                //console.log('newFact:' + newFact);

                this.directGraph.set(head.relation, []);//check recursion for fix
                //this.isDerivatedRelation.set()
                //this.directGraph.set('t', ['test']);//check recursion for fix

               // console.log("head" + head + "head.relation:" + head.relation + " directgraph: " + JSON.stringify(this.directGraph.get('t')));

                return newFact;
            }
            console.log("Literal:" + head +" should be a Fact literal!.")
            return null;
        }

        if (this.currentToken !== ':-') {
            //console.log(current);
            throw new SyntaxError('Expected :- in rule but got ' + this.currentToken);
        
        }
        this.nextToken(); // eat :-

        var listLiteral = [];

        while (this.currentToken !== '.') {
            listLiteral.push(this.parseLiteral());
            if (this.currentToken !== ',' && this.currentToken !== '.') {
                throw new SyntaxError('Expected , or ) in Literal but got ' + this.currentToken);
            }
            if (this.currentToken === ',') {
                this.nextToken(); // eat ,
            }
        }
        this.nextToken(); // eat .
        //var body=[];
        var listLiteralNames;
        //if (listLiteral.length === 1) {
        //    // body is a regular Literal
        //   // body.push(listLiteral[0]);
        //    listLiteralNames = [listLiteral[0].relation]
        //} else {
        //    // body is a conjunction of all Literals
        //    //body = new ConjunctionLiterals(listLiteral);
        //    listLiteralNames=body.getLiteralNames();
        //}
        var newRule = new Rule(head, listLiteral);
        listLiteralNames = newRule.getLiteralNames();

        if (this.directGraph.has(head.relation)) {
            var prevlistLiteralNames = this.directGraph.get(head.relation);
            listLiteralNames = listLiteralNames.concat(prevlistLiteralNames);
            //console.log("head" + head + "head.relation:" + head.relation + " directgraph: " + JSON.stringify(listLiteralNames));
        }
        this.directGraph.set(head.relation, listLiteralNames);

        //console.log("head" + head + "head.relation:" + head.relation + " directgraph: " + this.getDirectGraph() + "lenght:" + listLiteralNames.length);
        return newRule;
    }
    
    
     parseRules(): any {
         this.nextToken(); // start the tokens iterator
            var rules = [];
            while (!this.doneTokenList) {
                // each rule gets its own scope for variables
               this.variableScope = {};
                var parsedRule = this.parseRule();
                if (parsedRule) rules.push(parsedRule);
            }
            //console.log("RULES PARSED: \n\n" + rules.join('.\n') + '.');
            return rules;
     }

     parseQuery(): any {

            this.nextToken();
            this.variableScope = { };
            
            //console.log(parseLiteral());
            return this.parseLiteral();
        }
}