"use strict";
class QueryLiteral extends Literal {


    adorment: string;

    constructor(relation, listTerm, adorment) {
        super(relation, listTerm, true);
        this.adorment = adorment;

    }


    toString = function() {
        return 'query_' + this.relation + this.adorment+'(' + this.listTerm.join(',') + ')';
    };

}

