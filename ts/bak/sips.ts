"use strict";
class SIPS {

    adorment: string;
    rule: Rule;
 

    constructor(rule, adorment ) {

        this.rule = rule;
        this.adorment = adorment;
    
    }


    // p(X,Y,Z):-q(Z,W),r(X,Z),s(W,Y,X).
    // q(Z,W):-t(Z,W).

    //p(X,Y):-q(X,Z),r(Z,Y).
    //r(Z,Y):-t(Z,Y). 
    //    q(2,1).
    // t(1,2,3).



    // 
    //q(Y):-t(Y).
    //    p(X, Y):-s(X, Z), q(Y), p(Z, Y).

    //        p(X, Y):-r(X).

    //n(X):-not t(Y),r(X),not s(Y,X),q(Y).
  

    
    optimalRule(): any {

        var queryTerm = new Map();

        for (var i = 0; i < this.adorment.length; i++) {
            //console.log("terms:" + this.rule.head.listTerm + " adorment:" + this.adorment);
            var index = this.adorment[i];
            var term = <Variable>this.rule.head.listTerm[<any>index - 1];
           // console.log(" add term:" + term + " adorment:" + this.adorment);
            queryTerm.set(term.name,term);
        }
        //console.log("queryTerm:" + JSON.stringify([...queryTerm]));
        var newBody = [];
        var newNegativeBody = [];

       // console.log("processing rule:" + this.rule);

        for (let bodyliteral of this.rule.body) {

            var totalBind = 0;

            for (let term of bodyliteral.listTerm) {
                var variable = <Variable>term;
                if (queryTerm.has(variable.name)) {
                    totalBind++;

                   // console.log("variable.name:" + variable.name + " variable: " + variable);
                }
            }
            bodyliteral.totalBind = totalBind;
            if (bodyliteral.isPositive) {
                newBody.push(bodyliteral);
            }
            else
                newNegativeBody.push(bodyliteral);

            
        }
        var sortedSips: Literal[] = newBody.sort((n1, n2) => n2.totalBind - n1.totalBind);

        if (newNegativeBody.length > 0) {
            var sortedNegativeSips: Literal[] = newNegativeBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
            sortedSips = sortedSips.concat(sortedNegativeSips);

        }

        //console.log("SortedSips:" + sortedSips+" adorment:"+this.adorment);
        return sortedSips;

    };


    optimalRuleWithoutAdorment(directGraph): any { //adorment 0


        //console.log("queryTerm:" + JSON.stringify([...queryTerm]));
        var newBaseBody = [];
        var newDerivatedBody = [];
        var newNegativeBody = [];

        // console.log("processing rule:" + this.rule);

        for (let bodyliteral of this.rule.body) {

            bodyliteral.totalBind = bodyliteral.listTerm.length;

            if (bodyliteral.isPositive) {
                if (directGraph.has(bodyliteral.relation)) { //It is a derivated relation
                    newDerivatedBody.push(bodyliteral);
                }
                else //It is a base relation
                    newBaseBody.push(bodyliteral);
            }
            else
                newNegativeBody.push(bodyliteral);


        }
        var sortedSips: Literal[] = newBaseBody.sort((n1, n2) => n1.totalBind - n2.totalBind);

        if (newDerivatedBody.length > 0) {
            var sortedDerivatedSips: Literal[] = newDerivatedBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedDerivatedSips);

        }

        if (newNegativeBody.length > 0) {
            var sortedNegativeSips: Literal[] = newNegativeBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedNegativeSips);

        }

        //console.log("A0-SortedSips:" + sortedSips + " adorment:" + this.adorment);
        return sortedSips;

    };

    toString = function () {
        return 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
    };

}

