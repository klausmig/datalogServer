export declare namespace data {
    class Term {
        type: string;
        isConstant: boolean;
        constructor(type: any);
        toString: () => any;
    }
}
