/// <reference path="term4.d.ts" />
declare namespace datalog {
    class Variable extends Term {
        name: string;
        constructor(name: any);
        match: (other: any) => Map<any, any>;
        substitute: (bindings: any) => any;
        toString: () => any;
    }
}
