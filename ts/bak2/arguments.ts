"use strict";
class Arguments {
    listTerm;

    constructor(listTerm) {
        this.listTerm = listTerm;
    }

    substitute(bindings) {
        return new Arguments(this.listTerm.map(function(arg) {
            return arg.substitute(bindings);
        }));
    }

    toString() {
        return this.listTerm.join(',');
    }
}