declare class Term {
    type: string;
    isConstant: boolean;
    constructor(type: any);
    toString: () => any;
}
declare class Constant extends Term {
    value: string;
    constructor(value: any);
    toString: () => any;
}
declare class Variable extends Term {
    name: string;
    constructor(name: any);
    match: (other: any) => Map<any, any>;
    substitute: (bindings: any) => any;
    toString: () => any;
}
declare class Match {
    isMatched: boolean;
    bindingMap: any;
    constructor();
    getLiteralNames(): any[];
}
declare class Literal {
    relation: string;
    listTerm: Term[];
    totalBind: number;
    isPositive: boolean;
    isGround: boolean;
    constructor(relation: any, listTerm: any, isPositive: any);
    isFactLiteral(): boolean;
    match(otherLiteral: any): Map<Variable, Constant>;
    substitute: (bindings: any) => Literal;
    query: (database: any) => IterableIterator<any>;
    toString: () => any;
}
declare class Rule {
    head: Literal;
    body: Literal[];
    isFact: boolean;
    constructor(head: any, body: any);
    getLiteralNames(): any[];
    substituteBody(bindings: any): Literal[];
    queryBody: (database: any) => IterableIterator<any>;
    toString: () => any;
}
declare class SipsRule extends Rule {
    adorment: string;
    constructor(head: any, body: any, adorment: any);
    toStringWithAdorment: () => string;
}
declare class Database {
    rules: Rule[];
    constructor(rules: any);
    queryDB: (goal: any) => any[];
    query: (goal: any) => IterableIterator<any>;
    toString(): string;
    toHtml(): string;
    getRules(): Rule[];
}
declare var log: string[];
declare function lexer(text: any): IterableIterator<any>;
declare class DependencyGraph {
    name: string;
    directedGraph: any;
    invertDirectedGraph: any;
    layerNode: any;
    maxLayer: any;
    allNodes: any;
    nodes: any;
    edges: any;
    constructor();
    getDirectGraph(): string;
    fixLayers(changedLayer: any, deep: any): void;
    calculateInvertGraph(): void;
    calculateLayer(node: any, listBody: any): number;
    stratified(): void;
    analyze(): void;
    toString: () => any;
}
declare class Parser {
    tokens: any;
    currentToken: any;
    doneTokenList: any;
    variableScope: any;
    dg: any;
    errorParser: boolean;
    errorMsg: string;
    constructor(tokens: any);
    nextToken(): void;
    parseAtom(): string;
    parseTerm(): any;
    parseLiteral(): Literal;
    parseRule(): any;
    parseRules(): any;
    parseQuery(): any;
}
declare class AnswerLiteral extends Literal {
    adorment: string;
    constructor(relation: any, listTerm: any, isPositive: any, adorment: any);
    toString: () => string;
}
declare class QueryLiteral extends Literal {
    adorment: string;
    constructor(relation: any, listTerm: any, adorment: any);
    toString: () => string;
}
declare class SIPS {
    adorment: string;
    rule: Rule;
    sipsRule: SipsRule;
    constructor(rule: any, adorment: any);
    optimalRule(): any;
    optimalRuleWithoutAdorment(directGraph: any): any;
    toString: () => string;
}
declare class MagicRule {
    rule: Rule;
    queryRules: Rule[];
    answerRules: Rule[];
    sipsRules: Rule[];
    constructor(rule: any, queryRules: any, answerRules: any, sipsRules: any);
    toString: () => string;
}
declare class MagicSet {
    ruleDb: Database;
    directGraph: any;
    answerList: Rule[];
    queryList: Rule[];
    sipsList: SipsRule[];
    magicSet: MagicRule[];
    constructor(ruleDb: any, directGraph: any);
    getCombinations(chars: any): any;
    generateMS(): any;
    toString: () => string;
    SipsRulestoString: () => string;
}
export { Term };
export { Constant };
export { Variable };
export { Match };
export { Literal };
export { Rule };
export { SipsRule };
export { Database };
export { log };
export { lexer };
export { DependencyGraph };
export { Parser };
export { AnswerLiteral };
export { QueryLiteral };
export { SIPS };
export { MagicRule };
export { MagicSet };
