"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Term {
    constructor(type) {
        this.isConstant = false;
        this.toString = function () {
            return this.type;
        };
        this.type = type;
    }
}
exports.Term = Term;
"use strict";
class Constant extends Term {
    constructor(value) {
        super("Constant");
        this.toString = function () {
            return this.value;
        };
        this.value = value;
        this.isConstant = true;
    }
}
exports.Constant = Constant;
"use strict";
class Variable extends Term {
    constructor(name) {
        super('Variable');
        this.match = function (other) {
            var bindings = new Map;
            if (this !== other) {
                bindings.set(this, other);
            }
            return bindings;
        };
        this.substitute = function (bindings) {
            var value = bindings.get(this);
            if (value) {
                return value;
            }
            return this;
        };
        this.toString = function () {
            return this.name;
        };
        this.name = name;
    }
}
exports.Variable = Variable;
"use strict";
class Match {
    constructor() {
        this.isMatched = false;
        this.bindingMap = new Map();
    }
    ;
    getLiteralNames() {
        var literalNameList = [];
        //for (var item of this.body) {
        //    literalNameList.unshift(item.relation);
        //}
        return literalNameList;
    }
}
exports.Match = Match;
"use strict";
class Literal {
    constructor(relation, listTerm, isPositive) {
        this.isPositive = true;
        this.isGround = false;
        this.substitute = function (bindings) {
            var argSubstitute = this.listTerm.map(function (term) {
                if (term instanceof Variable) {
                    //console.log('Termtosubstitute:' + term + ' bindings:' + JSON.stringify([...bindings]));
                    return term.substitute(bindings);
                }
                else
                    return term; //It is a Constant
            });
            var newLiteralSustutute = new Literal(this.relation, argSubstitute, this.isPositive);
            return newLiteralSustutute;
        };
        this.query = function* (database) {
            yield* database.query(this);
        };
        this.toString = function () {
            if (this.listTerm.length === 0) {
                return this.relation;
            }
            if (this.isPositive)
                return this.relation + '(' + this.listTerm.join(',') + ')';
            else
                return " not " + this.relation + '(' + this.listTerm.join(',') + ')';
            //if (this.totalBind)
            //    return this.relation + '(' + this.listTerm.join(',') + ')' + this.totalBind;
            //else
        };
        this.relation = relation;
        this.listTerm = listTerm || [];
        this.isPositive = isPositive;
    }
    isFactLiteral() {
        for (var term of this.listTerm) {
            //console.log("validation:" + term + " type:" + term.type);
            if (term.type == "Variable") {
                this.isGround = false;
                return false;
            }
        }
        this.isGround = true;
        return true;
    }
    match(otherLiteral) {
        var matchObj = new Match();
        if (otherLiteral instanceof Literal) {
            if ((this.relation !== otherLiteral.relation) || (this.listTerm.length !== otherLiteral.listTerm.length)) {
                return null;
            }
            //console.log("actualliteral:" + this);
            //if (this.isGround) {
            //console.log("Actualliteral:" + this + " otherLiteral:" + otherLiteral);
            for (var i = 0; i < otherLiteral.listTerm.length; i++) {
                var term = this.listTerm[i];
                var otherTerm = otherLiteral.listTerm[i];
                // console.log('bindings:' + JSON.stringify([...matchObj.bindingMap]));
                if (!term.isConstant && otherTerm.isConstant) {
                    matchObj.bindingMap.set(term, otherTerm); // binding variable and constant
                }
                else if (term.isConstant && !otherTerm.isConstant) {
                    matchObj.bindingMap.set(otherTerm, term); // binding variable and constant
                }
                else if (term.isConstant && otherTerm.isConstant) {
                    if (term.value !== otherTerm.value) {
                        //console.log("turn nulldiff headterm:" + this.listTerm[i] + " goalterm:" + otherLiteral.listTerm[i]);
                        return null;
                    }
                    else {
                        matchObj.bindingMap.set(term, otherTerm);
                    }
                }
            }
            //console.log('Bindings:' + JSON.stringify([...matchObj.bindingMap]));
            return matchObj.bindingMap;
        }
    }
    ;
}
exports.Literal = Literal;
"use strict";
class Rule {
    constructor(head, body) {
        this.isFact = false;
        this.queryBody = function* (database) {
            var self = this;
            function* solutions(index, bindings) {
                var literal = self.body[index];
                if (!literal) {
                    //yield self.substituteBody(bindings);
                    yield self.head.substitute(bindings);
                }
                else {
                    //console.log("quering literal:" + literal.substitute(bindings) + "lit:" + literal);
                    if (!literal.isPositive) {
                        var positiveMatch = false;
                        for (var item of database.query(literal.substitute(bindings))) {
                            var newBinding = literal.match(item);
                            if (newBinding.size > 0) {
                                positiveMatch = true;
                                break;
                            }
                        }
                        if (!positiveMatch)
                            yield* solutions(index + 1, bindings);
                    }
                    else
                        for (var item of database.query(literal.substitute(bindings))) {
                            var newBinding = literal.match(item);
                            if (newBinding.size > 0) {
                                var unified = new Map([...bindings, ...literal.match(item)]); // concatenate the bindings for every term in the body
                                //console.log('unifiedBinding:' + JSON.stringify([...unified]));
                                if (unified) {
                                    yield* solutions(index + 1, unified);
                                }
                            }
                        }
                }
            }
            yield* solutions(0, new Map);
        };
        this.toString = function () {
            if (this.isFact)
                return this.head.toString();
            return this.head + ' :- ' + this.body;
        };
        this.head = head;
        this.body = body;
    }
    getLiteralNames() {
        var literalNameList = [];
        for (var item of this.body) {
            var relationName = item.relation;
            if (!item.isPositive)
                relationName = '~' + item.relation;
            literalNameList.unshift(relationName);
        }
        return literalNameList;
    }
    substituteBody(bindings) {
        // console.log('bodybindings:' + JSON.stringify([...bindings]));
        return this.body.map(function (literal) {
            return literal.substitute(bindings);
        });
    }
    ;
}
exports.Rule = Rule;
"use strict";
class SipsRule extends Rule {
    constructor(head, body, adorment) {
        super(head, body);
        this.toStringWithAdorment = function () {
            return this.adorment + ": " + this.head + ' :- ' + this.body;
        };
        this.adorment = adorment;
    }
}
exports.SipsRule = SipsRule;
"use strict";
class Database {
    constructor(rules) {
        this.queryDB = function (goal) {
            var results = [];
            for (var item of this.query(goal)) {
                results.push(item + "");
            }
            var uniqueResult = Array.from(new Set(results));
            //console.log(JSON.stringify(uniqueResult));
            return (uniqueResult);
        };
        this.query = function* (goal) {
            for (var i = 0, rule; rule = this.rules[i]; i++) {
                var match = rule.head.match(goal);
                if (match) {
                    //console.log("rulein:"+rule);
                    //console.log("head:" + rule.head + " goal:" + goal + " match:" + JSON.stringify([...match]));
                    var head = rule.head.substitute(match);
                    //console.log("head-subs:" + head);
                    if (!rule.isFact) {
                        var body = rule.substituteBody(match);
                        //console.log("body-subs:" + body);
                        //console.log("rulesust:"+rule);
                        var newQueryRule = new Rule(head, body);
                        //throw new Error("Debug");
                        for (var item of newQueryRule.queryBody(this)) {
                            //console.log("item:" + item + " map:" + JSON.stringify([...head.match(item)]));
                            // yield head.substitute(head.match(item));
                            yield item;
                            //  throw new Error("Debug");
                        }
                    }
                    else
                        yield head;
                }
            }
        };
        this.rules = rules;
    }
    toString() {
        return this.rules.join('.\n') + '.';
    }
    toHtml() {
        return this.rules.join('.<br/>') + '.';
    }
    getRules() {
        return this.rules;
    }
}
exports.Database = Database;
var log = [];
exports.log = log;
function* lexer(text) {
    var tokenRegexp = /[A-Za-z_\d]+|:\-|[()\.,~]/g;
    var match;
    while ((match = tokenRegexp.exec(text)) !== null) {
        //console.log("match:"+match[0]);
        yield match[0];
    }
}
exports.lexer = lexer;
"use strict";
class DependencyGraph {
    constructor() {
        this.toString = function () {
            return this.name;
        };
        this.directedGraph = new Map();
        this.invertDirectedGraph = new Map();
        this.layerNode = new Map();
        this.allNodes = new Map();
        this.nodes = [];
        this.edges = [];
    }
    getDirectGraph() {
        var mapStr = "";
        for (let pair of this.directedGraph) {
            //console.log(pair[0] + "[" + pair[1].join(',') + "]");
            mapStr += pair[0] + "[" + pair[1].join(',') + "]";
        }
        return mapStr;
    }
    fixLayers(changedLayer, deep) {
        //if (deep== 2) return;
        for (let relationLayer of changedLayer) {
            var listRelation = this.invertDirectedGraph.get(relationLayer);
            var numLayer = this.layerNode.get(relationLayer);
            console.log('L' + numLayer + 'root:' + relationLayer + ' lists:' + listRelation);
            if (listRelation)
                for (let relationBody of listRelation) {
                    if (this.layerNode.get(relationBody) <= numLayer) {
                        if (this.maxLayer < numLayer + 1)
                            this.maxLayer = numLayer + 1;
                        this.layerNode.set(relationBody, numLayer + 1);
                        //this.fixLayers([relationBody], ++deep);
                    }
                }
        }
        return;
    }
    calculateInvertGraph() {
        for (let pair of this.directedGraph) {
            var node = pair[0];
            var listBody = pair[1];
            for (let edgeNode of listBody) {
                if ((edgeNode.substring(0, 1) == "~") || (listBody.indexOf('~' + edgeNode) > -1)) {
                    edgeNode = edgeNode.replace(/[~]/gi, '');
                }
                if (this.invertDirectedGraph.has(edgeNode)) {
                    var listRelation = this.invertDirectedGraph.get(edgeNode);
                    //console.log([...this.invertDirectedGraph]);
                    if (edgeNode != node) {
                        listRelation.push(node);
                        this.invertDirectedGraph.set(edgeNode, listRelation);
                    }
                }
                else if (edgeNode != node) {
                    this.invertDirectedGraph.set(edgeNode, [node]);
                }
            }
        }
        //console.log('\n\ninvertDirectedGraph:\n');
        //for (let pair of this.invertDirectedGraph) {
        //    console.log('relation:' + pair[0] + ' [' + pair[1].join(',') + ']');
        //}
    }
    calculateLayer(node, listBody) {
        var safeLayer = true;
        var maxLayer = 0;
        for (let edgeNode of listBody) {
            if ((edgeNode.substring(0, 1) == "~") || (listBody.indexOf('~' + edgeNode) > -1)) {
                edgeNode = edgeNode.replace(/[~]/gi, '');
            }
            if (node != edgeNode)
                if (this.layerNode.has(edgeNode)) {
                    if (this.layerNode.get(edgeNode) > maxLayer)
                        maxLayer = this.layerNode.get(edgeNode);
                }
                else {
                    //if (edgeNode != pair[0])
                    //    if (maxLayer == 0) {
                    //        this.layerNode.set(edgeNode, ++maxLayer);
                    //    } else
                    //        this.layerNode.set(edgeNode, maxLayer);
                    safeLayer = false;
                }
        }
        if (safeLayer)
            return maxLayer + 1;
        else
            return -1;
    }
    stratified() {
        this.calculateInvertGraph();
        for (let pair of this.allNodes) {
            //console.log('node:' + pair[0] );
            if (!this.directedGraph.has(pair[0])) {
                this.layerNode.set(pair[0], 0);
            }
        }
        this.maxLayer = 0;
        var withLayer = [];
        for (let pair of this.directedGraph) {
            //           console.log(pair[0] + "[" + pair[1].join(',') + "]");
            var maxLayer = 0;
            var node = pair[0];
            var numLayer = this.calculateLayer(node, pair[1]);
            if (numLayer > 0) {
                this.layerNode.set(node, numLayer);
                withLayer.push(node);
                if (this.maxLayer < numLayer)
                    this.maxLayer = numLayer;
            }
            //console.log('node:' + node + " layer:" + numLayer);
            //if (this.layerNode.has(pair[0])) {
            //    //console.log("has node:" + pair[0]);
            //    if (this.layerNode.get(pair[0]) < maxLayer + 1) {
            //        changedLayer.push(pair[0]);
            //        this.layerNode.set(pair[0], maxLayer + 1)
            //        if (this.maxLayer < maxLayer + 1)
            //            this.maxLayer = maxLayer + 1;
            //    }
            //} else {
            //    this.layerNode.set(pair[0], maxLayer + 1);
            //    if (this.maxLayer < maxLayer + 1)
            //        this.maxLayer = maxLayer + 1;
            //}
        }
        //console.log('\n\witlayer:\n');
        //for (let pair of withLayer) {
        //    console.log('relation:' + pair);
        // }
        var i = 0;
        while (withLayer.length > 0) {
            i++;
            var nodeWithLayer = withLayer.shift();
            if (this.invertDirectedGraph.has(nodeWithLayer)) {
                var listBody = this.invertDirectedGraph.get(nodeWithLayer);
                for (let edgeNode of listBody) {
                    if (!this.layerNode.has(edgeNode)) {
                        var numLayer = this.calculateLayer(edgeNode, this.directedGraph.get(edgeNode));
                        if (numLayer > 0) {
                            this.layerNode.set(edgeNode, numLayer);
                            withLayer.push(edgeNode);
                            if (this.maxLayer < numLayer)
                                this.maxLayer = numLayer;
                        }
                    }
                }
            }
            if (i == 100)
                break;
        }
        //console.log('\n\layerNode:\n');
        //for (let pair of this.layerNode) {
        //    console.log('relation:' + pair[0] + ' layer:' + pair[1]);
        //}
    }
    analyze() {
        this.stratified();
        for (let pair of this.directedGraph) {
            //           console.log(pair[0] + "[" + pair[1].join(',') + "]");
            for (let edgeNode of pair[1]) {
                var label = "";
                var style = "";
                if ((edgeNode.substring(0, 1) == "~") || (pair[1].indexOf('~' + edgeNode) > -1)) {
                    edgeNode = edgeNode.replace(/[~]/gi, '');
                    label = "not";
                    style = "critical";
                }
                //console.log(pair[0] + ' indexOf:' + pair[1].indexOf('~' + edgeNode) + ' edge: +~' + edgeNode);
                if (pair[0] == edgeNode) {
                    style = "important";
                }
                this.edges.unshift({
                    group: "edges",
                    data: {
                        id: pair[0] + edgeNode,
                        source: pair[0],
                        target: edgeNode,
                        label: label
                    },
                    classes: style
                });
            }
            //    console.log('this.maxLayer:' + this.maxLayer)
        }
        //console.log('changedLayer:' + changedLayer + ' invertDirectedGraph:' + [... this.invertDirectedGraph]);
        // this.fixLayers(changedLayer,1);
        for (let pair of this.allNodes) {
            //console.log('node:' + pair[0] );
            this.nodes.unshift({
                group: "nodes",
                data: {
                    id: pair[0]
                },
                classes: "l" + this.layerNode.get(pair[0])
            });
        }
        // console.log("layer:[" + [...this.layerNode] + "]");
    }
}
exports.DependencyGraph = DependencyGraph;
"use strict";
class Parser {
    constructor(tokens) {
        //isDerivatedRelation;
        this.errorParser = false;
        this.errorMsg = "";
        this.tokens = tokens;
        this.dg = new DependencyGraph();
        //this.isDerivatedRelation = new Map();
        //console.log("cont tokens:" + this.tokens);
    }
    nextToken() {
        var nextTokenList = this.tokens.next();
        this.currentToken = nextTokenList.value;
        this.doneTokenList = nextTokenList.done;
    }
    parseAtom() {
        var name = this.currentToken;
        if (!/^[A-Za-z_\d]+$/.test(name)) {
            //throw new SyntaxError('Bad atom name: ' + name);
            log.push("Bad atom name: " + name);
            this.errorParser = true;
            return null;
        }
        this.nextToken();
        return name;
    }
    parseTerm() {
        var name = this.parseAtom();
        if (/^[A-Z_][A-Za-z_\d]*$/.test(name)) {
            if (name === '_') {
                return new Variable('_');
            }
            // variable X in the same scope should point to the same object
            var variable = this.variableScope[name];
            if (!variable) {
                variable = this.variableScope[name] = new Variable(name);
            }
            return variable;
        }
        return new Constant(name);
    }
    parseLiteral() {
        //console.log("Parsing Literal ...");
        //console.log("this.currentToken:" + this.currentToken);
        var isPositiveLiteral = true;
        if ((this.currentToken === 'not') || (this.currentToken === '~')) {
            this.nextToken();
            isPositiveLiteral = false;
        }
        //console.log("this.currentToken:" + this.currentToken);
        var name = this.parseAtom();
        // var arguments: Arguments;
        if (this.currentToken === '(') {
            //console.log("this.currentToken:" + this.currentToken);
            this.nextToken(); // eat (
            //console.log("this.currentToken:" + this.currentToken);
            var listTerm = [];
            while (this.currentToken !== ')') {
                listTerm.push(this.parseTerm());
                if (this.currentToken !== ',' && this.currentToken !== ')') {
                    //throw new SyntaxError('Expected , or ) in Literal but got ' + this.currentToken);
                    log.push("Expected , or ) in Literal but got: " + this.currentToken);
                    this.errorParser = true;
                    return null;
                }
                if (this.currentToken === ',') {
                    this.nextToken(); // eat ,
                }
                //console.log("this.currentToken:" + this.currentToken);
            }
            this.nextToken(); // eat )
            // arguments = new Arguments(listTerm);
            //console.log("this.currentToken:" + this.currentToken);
        }
        //console.log("literal:" + name + listTerm + isPositiveLiteral);
        return new Literal(name, listTerm, isPositiveLiteral);
    }
    parseRule() {
        var head = this.parseLiteral();
        //console.log('head:'+head);
        if (this.currentToken === '.') {
            this.nextToken(); // eat .
            // return new Rule(head, Literal.TRUE);
            if (head.isFactLiteral()) {
                var newFact = new Rule(head, []);
                newFact.isFact = true;
                //console.log('newFact:' + newFact);
                this.dg.directedGraph.set(head.relation, []); //check recursion for fix
                //this.isDerivatedRelation.set()
                //this.directGraph.set('t', ['test']);//check recursion for fix
                // console.log("head" + head + "head.relation:" + head.relation + " directgraph: " + JSON.stringify(this.directGraph.get('t')));
                return newFact;
            }
            console.log("Literal:" + head + " should be a Fact literal!.");
            log.push("Literal:" + head + " should be a Fact literal!.");
            this.errorParser = true;
            return null;
        }
        if (this.currentToken !== ':-') {
            //console.log(current);
            //throw new SyntaxError('Expected :- in rule but got ' + this.currentToken);
            log.push("Expected :- in rule but got  " + this.currentToken);
            this.errorParser = true;
            return null;
        }
        this.nextToken(); // eat :-
        var listLiteral = [];
        while (this.currentToken !== '.') {
            listLiteral.push(this.parseLiteral());
            if (this.currentToken !== ',' && this.currentToken !== '.') {
                //throw new SyntaxError('Expected , or ) in Literal but got ' + this.currentToken);
                log.push("Expected , or ) in Literal but got  " + this.currentToken);
                this.errorParser = true;
                return null;
            }
            if (this.currentToken === ',') {
                this.nextToken(); // eat ,
            }
        }
        this.nextToken(); // eat .
        //var body=[];
        var listLiteralNames;
        var newRule = new Rule(head, listLiteral);
        listLiteralNames = newRule.getLiteralNames();
        for (let relationInBody of listLiteralNames) {
            if (relationInBody.substring(0, 1) == "~") {
                relationInBody = relationInBody.replace(/[~]/gi, '');
            }
            this.dg.allNodes.set(relationInBody, true);
        }
        if (this.dg.directedGraph.has(head.relation)) {
            var prevlistLiteralNames = this.dg.directedGraph.get(head.relation);
            listLiteralNames = listLiteralNames.concat(prevlistLiteralNames);
            //console.log("head" + head + "head.relation:" + head.relation + " directgraph: " + JSON.stringify(listLiteralNames));
        }
        this.dg.directedGraph.set(head.relation, listLiteralNames);
        this.dg.allNodes.set(head.relation, true);
        //console.log("head" + head + "head.relation:" + head.relation + " directgraph: " + this.getDirectGraph() + "lenght:" + listLiteralNames.length);
        return newRule;
    }
    parseRules() {
        this.nextToken(); // start the tokens iterator
        var rules = [];
        while (!this.doneTokenList) {
            // each rule gets its own scope for variables
            this.variableScope = {};
            var parsedRule = this.parseRule();
            if (parsedRule)
                rules.push(parsedRule);
        }
        //console.log("RULES PARSED: \n\n" + rules.join('.\n') + '.');
        return rules;
    }
    parseQuery() {
        this.nextToken();
        this.variableScope = {};
        //console.log(parseLiteral());
        return this.parseLiteral();
    }
}
exports.Parser = Parser;
"use strict";
class AnswerLiteral extends Literal {
    constructor(relation, listTerm, isPositive, adorment) {
        super(relation, listTerm, isPositive);
        this.toString = function () {
            if (this.isPositive)
                return 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
            else
                return " not " + 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
        };
        this.adorment = adorment;
    }
}
exports.AnswerLiteral = AnswerLiteral;
"use strict";
class QueryLiteral extends Literal {
    constructor(relation, listTerm, adorment) {
        super(relation, listTerm, true);
        this.toString = function () {
            if (this.adorment == "0") {
                return 'query_' + this.relation;
            }
            return 'query_' + this.relation + this.adorment + '(' + this.listTerm.join(',') + ')';
        };
        this.adorment = adorment;
    }
}
exports.QueryLiteral = QueryLiteral;
"use strict";
class SIPS {
    constructor(rule, adorment) {
        this.toString = function () {
            return 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
        };
        this.rule = rule;
        this.adorment = adorment;
    }
    optimalRule() {
        var queryTerm = new Map();
        for (var i = 0; i < this.adorment.length; i++) {
            //console.log("terms:" + this.rule.head.listTerm + " adorment:" + this.adorment);
            var index = this.adorment[i];
            var term = this.rule.head.listTerm[index - 1];
            // console.log(" add term:" + term + " adorment:" + this.adorment);
            queryTerm.set(term.name, term);
        }
        //console.log("queryTerm:" + JSON.stringify([...queryTerm]));
        var newBody = [];
        var newNegativeBody = [];
        var unboundBody = [];
        var unboundNegativeBody = [];
        // console.log("processing rule:" + this.rule);
        for (let bodyliteral of this.rule.body) {
            var totalBind = 0;
            for (let term of bodyliteral.listTerm) {
                var variable = term;
                if (queryTerm.has(variable.name)) {
                    totalBind++;
                    // console.log("variable.name:" + variable.name + " variable: " + variable);
                }
            }
            bodyliteral.totalBind = totalBind;
            // console.log("bodyliteral:" + bodyliteral + " bodyliteral.totalBind: " + bodyliteral.totalBind);
            if (totalBind == 0)
                if (bodyliteral.isPositive) {
                    unboundBody.push(bodyliteral);
                }
                else
                    unboundNegativeBody.push(bodyliteral);
            else if (bodyliteral.isPositive) {
                newBody.push(bodyliteral);
            }
            else
                newNegativeBody.push(bodyliteral);
        }
        var sortedSips = newBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
        if (unboundBody.length > 0) {
            var unifiedTerm = new Map();
            for (let boundBodyliteral of sortedSips) {
                for (let term of boundBodyliteral.listTerm) {
                    var variable = term;
                    unifiedTerm.set(variable.name, true);
                }
            }
            //console.log("unifiedTerm" + [...unifiedTerm]);
            var newBoundBody = [];
            for (let unboundBodyliteral of unboundBody) {
                var totalBind = 0;
                for (let term of unboundBodyliteral.listTerm) {
                    var variable = term;
                    if (unifiedTerm.has(variable.name)) {
                        totalBind++;
                        // console.log("variable.name:" + variable.name + " variable: " + variable);
                    }
                }
                unboundBodyliteral.totalBind = totalBind;
                //console.log("unboundBodyliteral:" + unboundBodyliteral + " unboundBodyliteral.totalBind: " + unboundBodyliteral.totalBind);
                newBoundBody.push(unboundBodyliteral);
            }
            var sortedUnboundSips = newBoundBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
            //console.log("sortedUnboundSips" + sortedUnboundSips);
            sortedSips = sortedSips.concat(sortedUnboundSips);
        }
        if (newNegativeBody.length > 0) {
            var sortedNegativeSips = newNegativeBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
            // console.log("neg SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedNegativeSips);
            sortedSips = sortedSips.concat(sortedNegativeSips);
        }
        if (unboundNegativeBody.length > 0) {
            var sortedNegativeSips = unboundNegativeBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
            //console.log("unneg SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedNegativeSips);
            sortedSips = sortedSips.concat(sortedNegativeSips);
        }
        //console.log("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        log.push("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        this.sipsRule = new SipsRule(this.rule.head, sortedSips, this.rule.head.relation + this.adorment);
        //console.log("sipsrule:" + this.sipsRule);
        return sortedSips;
    }
    ;
    optimalRuleWithoutAdorment(directGraph) {
        //console.log("queryTerm:" + JSON.stringify([...queryTerm]));
        var newBaseBody = [];
        var newDerivatedBody = [];
        var newNegativeBody = [];
        // console.log("processing rule:" + this.rule);
        for (let bodyliteral of this.rule.body) {
            bodyliteral.totalBind = bodyliteral.listTerm.length;
            if (bodyliteral.isPositive) {
                if (directGraph.has(bodyliteral.relation)) {
                    newDerivatedBody.push(bodyliteral);
                }
                else
                    newBaseBody.push(bodyliteral);
            }
            else
                newNegativeBody.push(bodyliteral);
        }
        var sortedSips = newBaseBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
        if (newDerivatedBody.length > 0) {
            var sortedDerivatedSips = newDerivatedBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedDerivatedSips);
        }
        if (newNegativeBody.length > 0) {
            var sortedNegativeSips = newNegativeBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedNegativeSips);
        }
        // console.log("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        log.push("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        this.sipsRule = new SipsRule(this.rule.head, sortedSips, this.rule.head.relation);
        // console.log("sipsrule:" + this.sipsRule);
        return sortedSips;
    }
    ;
} //"use strict";
exports.SIPS = SIPS;
class MagicRule {
    constructor(rule, queryRules, answerRules, sipsRules) {
        this.toString = function () {
            return this.rule + "\n [" + this.queryRules.join("\n") + "]" + "\n [" + this.answerRules.join("\n") + "]\n";
        };
        this.rule = rule;
        this.queryRules = queryRules;
        this.answerRules = answerRules;
        this.sipsRules = sipsRules;
    }
}
exports.MagicRule = MagicRule;
"use strict";
class MagicSet {
    constructor(ruleDb, directGraph) {
        this.toString = function () {
            return this.queryList.join('.\n') + '.' + '\n\n' + this.answerList.join('.\n') + '.';
        };
        this.SipsRulestoString = function () {
            var str = "";
            console.log("SipsRulestoString:" + this.sipsList);
            for (var i = 0; i < this.sipsList.length; i++) {
                str += this.sipsList[i].toStringWithAdorment() + "\n";
            }
            return str;
        };
        this.ruleDb = ruleDb;
        this.directGraph = directGraph;
    }
    getCombinations(chars) {
        var result = [];
        var f = function (prefix, chars) {
            for (var i = 0; i < chars.length; i++) {
                result.push(prefix + chars[i]);
                f(prefix + chars[i], chars.slice(i + 1));
            }
        };
        f('', chars);
        return result;
    }
    generateMS() {
        this.answerList = [];
        this.queryList = [];
        this.sipsList = [];
        this.magicSet = [];
        for (let rule of this.ruleDb.getRules()) {
            var magicRule = new MagicRule(rule, [], [], []);
            var listAdormentStr = "";
            for (var i = 1; i <= rule.head.listTerm.length; i++) {
                listAdormentStr += '' + i;
            }
            //console.log("adorment:" + listAdormentStr);
            //if (listAdormentStr.length > 1) {
            var listAdorment = this.getCombinations(listAdormentStr);
            listAdorment.push('0');
            //console.log("adormentCombinations:" + listAdorment);
            for (let adorment of listAdorment) {
                //console.log("1processing rule:" + rule);
                var unifiedVar = new Map();
                var sipsBody;
                var sipsObj = new SIPS(rule, adorment);
                if (adorment != '0') {
                    sipsBody = sipsObj.optimalRule();
                    this.sipsList.push(sipsObj.sipsRule);
                    magicRule.sipsRules.push(sipsObj.sipsRule);
                }
                else {
                    sipsBody = sipsObj.optimalRuleWithoutAdorment(this.directGraph);
                    this.sipsList.push(sipsObj.sipsRule);
                    magicRule.sipsRules.push(sipsObj.sipsRule);
                }
                var listTermQuery = [];
                //console.log("terms:" + rule.head.listTerm + " adorment:" + adorment);
                if (adorment != '0')
                    for (var i = 0; i < adorment.length; i++) {
                        var term = rule.head.listTerm[adorment[i] - 1];
                        listTermQuery.push(term);
                        unifiedVar.set(term.name, true);
                        //console.log("terms:" + rule.head.listTerm + " adorment:" + adorment);
                    }
                //console.log("listTermQuery:" + listTermQuery);
                var answerHead = new AnswerLiteral(rule.head.relation, rule.head.listTerm, rule.head.isPositive, adorment);
                var querySeed = new QueryLiteral(rule.head.relation, listTermQuery, adorment);
                var listLiteral = [];
                listLiteral.push(querySeed);
                for (let bodyliteral of sipsBody) {
                    var newLiteral;
                    var queryLiteral;
                    //console.log("  sipsBody:" + sipsBody + " bodyliteral:" + bodyliteral );
                    if (this.directGraph.has(bodyliteral.relation)) {
                        var listTermQueryLiteral = [];
                        var adormentQuery = "";
                        //  console.log("unifiedVarmap:" + JSON.stringify([...unifiedVar]));
                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {
                            var term = bodyliteral.listTerm[i];
                            if (unifiedVar.has(term.name)) {
                                listTermQueryLiteral.push(term);
                                adormentQuery += "" + (i + 1);
                                //      console.log("  bodyliteral.listTerm:" + bodyliteral.listTerm + " term:" + term + " adormentQuery:" + adormentQuery);
                            }
                        }
                        newLiteral = new AnswerLiteral(bodyliteral.relation, bodyliteral.listTerm, bodyliteral.isPositive, adormentQuery);
                        queryLiteral = new QueryLiteral(bodyliteral.relation, listTermQueryLiteral, adormentQuery);
                        // console.log("  AnswerLiteral:" + newLiteral + "  Positive:" + newLiteral.isPositive + " queryLiteral:" + queryLiteral);
                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {
                            var term = bodyliteral.listTerm[i];
                            //console.log("aca- terms:" + bodyliteral.listTerm + " term.name added:" + term.name + " adorment:" + adorment);
                            unifiedVar.set(term.name, true);
                        }
                        var newlistLiteral = listLiteral.slice();
                        var MSQueryRule = new Rule(queryLiteral, newlistLiteral);
                        //if (!adorment0Added) {
                        //    var querySeed0 = new QueryLiteral(rule.head.relation, [], '0');
                        //}
                        this.queryList.push(MSQueryRule);
                        magicRule.queryRules.push(MSQueryRule);
                        //console.log("MSQueryRule:" + MSQueryRule);
                    }
                    else {
                        newLiteral = bodyliteral;
                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {
                            var term = bodyliteral.listTerm[i];
                            //console.log("aca2-terms:" + bodyliteral.listTerm + " term.name added:" + term.name + " adorment:" + adorment);
                            unifiedVar.set(term.name, true);
                        }
                    }
                    listLiteral.push(newLiteral);
                }
                var MSAnswerRule = new Rule(answerHead, listLiteral);
                this.answerList.push(MSAnswerRule);
                magicRule.answerRules.push(MSAnswerRule);
                //console.log("MSRule:" + MSAnswerRule);
            }
            this.magicSet.push(magicRule);
        }
    }
}
exports.MagicSet = MagicSet;
