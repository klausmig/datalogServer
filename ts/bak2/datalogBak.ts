export namespace data {//"use strict";

export class Term {

	type: string;
	isConstant: boolean = false;

	constructor(type) {
		this.type = type;
	}

	toString = function() {
		return this.type;
	};


}"use strict";
export class Constant extends Term {

	value: string;

	constructor(value) {
		super("Constant");
		this.value = value;
		this.isConstant = true;
	}

	toString = function() {
		return this.value;
	};
}"use strict";

export class Variable extends Term {

    name: string;

    constructor(name) {
        super('Variable');
        this.name = name;

    }

    match = function(other) {
        var bindings = new Map;
        if (this !== other) {
            bindings.set(this, other);
        }
        return bindings;
    };

    substitute = function(bindings) {
        var value = bindings.get(this);
        if (value) {
            return value;
        }
        return this;
    };

    toString = function() {
        return this.name;
    };
}﻿"use strict";
export class Match {

    isMatched: boolean = false;;
    bindingMap: any;


    constructor() {
        this.bindingMap = new Map();
    }

    getLiteralNames() {
        var literalNameList = []
            //for (var item of this.body) {
            //    literalNameList.unshift(item.relation);
            //}
        return literalNameList;
    }
}"use strict";

export class Literal {
    relation: string;
    listTerm: Term[];
    totalBind: number;
    isPositive: boolean = true;
    isGround: boolean = false;

    constructor(relation, listTerm, isPositive) {
        this.relation = relation;
        this.listTerm = listTerm || [];
        this.isPositive = isPositive;
    }

    isFactLiteral(): boolean {
        for (var term of this.listTerm) {
            //console.log("validation:" + term + " type:" + term.type);
            if (term.type == "Variable") {

                this.isGround = false;
                return false;
            }
        }
        this.isGround = true;
        return true;
    }

    match(otherLiteral): Map < Variable, Constant > {

        var matchObj = new Match();

        if (otherLiteral instanceof Literal) {
            if ((this.relation !== otherLiteral.relation) || (this.listTerm.length !== otherLiteral.listTerm.length)) {
                return null;
            }
            //console.log("actualliteral:" + this);
            //if (this.isGround) {
            //console.log("Actualliteral:" + this + " otherLiteral:" + otherLiteral);
            for (var i = 0; i < otherLiteral.listTerm.length; i++) {

                var term = this.listTerm[i];
                var otherTerm = otherLiteral.listTerm[i];
               // console.log('bindings:' + JSON.stringify([...matchObj.bindingMap]));
                if (!term.isConstant && otherTerm.isConstant) {
                    matchObj.bindingMap.set( < Variable > term, < Constant > otherTerm); // binding variable and constant
                } else
                if (term.isConstant && !otherTerm.isConstant) {
                    matchObj.bindingMap.set( < Variable > otherTerm, < Constant > term); // binding variable and constant
                } else
                if (term.isConstant && otherTerm.isConstant) {
                    if (( < Constant > term).value !== ( < Constant > otherTerm).value) { // compare constant value must be equal for match
                        //console.log("turn nulldiff headterm:" + this.listTerm[i] + " goalterm:" + otherLiteral.listTerm[i]);
                        return null;
                    } else {
                        matchObj.bindingMap.set( < Constant > term, < Constant > otherTerm);
                    }
                }
            }
            //console.log('Bindings:' + JSON.stringify([...matchObj.bindingMap]));
            return matchObj.bindingMap;
        }

    };

    substitute = function(bindings) {

        var argSubstitute = this.listTerm.map(function(term) {

            if (term instanceof Variable) {
                //console.log('Termtosubstitute:' + term + ' bindings:' + JSON.stringify([...bindings]));
                return term.substitute(bindings);
            } else
                return term; //It is a Constant
        });

        var newLiteralSustutute = new Literal(this.relation, argSubstitute, this.isPositive);

        return newLiteralSustutute;
    };

    query = function*(database) {
        yield * database.query(this);
    };

    toString = function() {
        if (this.listTerm.length === 0) {
            return this.relation;
        }

        if (this.isPositive)
            return this.relation + '(' + this.listTerm.join(',') + ')';
        else
            return " not " + this.relation + '(' + this.listTerm.join(',') + ')';
        //if (this.totalBind)
        //    return this.relation + '(' + this.listTerm.join(',') + ')' + this.totalBind;
        //else

    };

}"use strict";
export class Rule {

    head: Literal;
    body: Literal[];
    isFact: boolean = false;

    constructor(head, body) {
        this.head = head;
        this.body = body;
    }

    getLiteralNames() {
        var literalNameList = []
        for (var item of this.body) {

            var relationName = item.relation;
            if (!item.isPositive)
                relationName = '~' + item.relation;

            literalNameList.unshift(relationName);
        }
        return literalNameList;
    }


    substituteBody(bindings) {
        // console.log('bodybindings:' + JSON.stringify([...bindings]));
        return this.body.map(function(literal) {
            return literal.substitute(bindings);
        });
    };

    queryBody = function*(database) {
        var self = this;

        function* solutions(index, bindings) {
            var literal = self.body[index];
            if (!literal) {
                //yield self.substituteBody(bindings);
                yield self.head.substitute(bindings);
            } else {
                //console.log("quering literal:" + literal.substitute(bindings) + "lit:" + literal);
                if (!literal.isPositive) { //negative Literal
                    var positiveMatch = false;
                    for (var item of database.query(literal.substitute(bindings))) { //check all items from DB matched with the literal
                        var newBinding = literal.match(item);
                        if (newBinding.size > 0) { //no matching for next term in body
                            positiveMatch = true;
                            break;
                        }
                    }

                    if (!positiveMatch)
                        yield * solutions(index + 1, bindings);
                } else //Positive Literal
                    for (var item of database.query(literal.substitute(bindings))) {
                    var newBinding = literal.match(item);
                    if (newBinding.size > 0) { //no matching for next term in body
                        var unified = new Map([...bindings, ...literal.match(item)]); // concatenate the bindings for every term in the body
                        //console.log('unifiedBinding:' + JSON.stringify([...unified]));
                        if (unified) {
                            yield * solutions(index + 1, unified);
                        }
                    }
                }
            }
        }
        yield * solutions(0, new Map);

    }


    toString = function() {

        if (this.isFact)
            return this.head.toString();
        return this.head + ' :- ' + this.body;
    };

}"use strict";

export class SipsRule extends Rule {

	adorment: string;

	constructor(head, body, adorment) {
		super(head, body);
		this.adorment = adorment;
	}

	toStringWithAdorment = function() {

		return this.adorment + ": " + this.head + ' :- ' + this.body;

	};

}"use strict";
export class Database {
    rules: Rule[];

    constructor(rules) {
        this.rules = rules;
    }

    queryDB = function (goal) {
        var results =  [];
        for (var item of this.query(goal)) {
            results.push(item + "");
        }

        var uniqueResult = Array.from(new Set(results));
        //console.log(JSON.stringify(uniqueResult));
        return (uniqueResult);

    }

    query = function*(goal) {
        for (var i = 0, rule; rule = this.rules[i]; i++) {

            var match = rule.head.match(goal);

            if (match) {
                //console.log("rulein:"+rule);
                //console.log("head:" + rule.head + " goal:" + goal + " match:" + JSON.stringify([...match]));
                var head = rule.head.substitute(match);
                //console.log("head-subs:" + head);

                if (!rule.isFact) {

                    var body = rule.substituteBody(match);
                    //console.log("body-subs:" + body);
                    //console.log("rulesust:"+rule);
                    var newQueryRule = new Rule(head, body);
                    //throw new Error("Debug");
                    for (var item of newQueryRule.queryBody(this)) {
                        //console.log("item:" + item + " map:" + JSON.stringify([...head.match(item)]));
                        // yield head.substitute(head.match(item));
                        yield item;
                        //  throw new Error("Debug");
                    }
                } else
                    yield head;
            }
        }
    };

    toString() {
        return this.rules.join('.\n') + '.';
    }

    toHtml() {
        return this.rules.join('.<br/>') + '.';
    }

    getRules() {
        return this.rules;
    }
}
export var log: string[] = [];

export function* lexer(text) {

	var tokenRegexp = /[A-Za-z_\d]+|:\-|[()\.,~]/g;
	var match;
	while ((match = tokenRegexp.exec(text)) !== null) {
		//console.log("match:"+match[0]);
		yield match[0];
	}
}"use strict";
export class Parser {

    tokens: any;
    currentToken: any;
    doneTokenList: any;
    variableScope;
    directGraph;
    allNodes;
    isDerivatedRelation;
    nodes: any;
    edges: any;
    errorParser: boolean = false;
    errorMsg: string = "";

    constructor(tokens) {
        this.tokens = tokens;
        this.directGraph = new Map();
        this.allNodes = new Map();
        this.isDerivatedRelation = new Map();
        this.nodes = [];
        this.edges = [];
        //console.log("cont tokens:" + this.tokens);

    }

    nextToken(): void {
        var nextTokenList = this.tokens.next();
        this.currentToken = nextTokenList.value;
        this.doneTokenList = nextTokenList.done;

    }
    getDirectGraph() {

        var mapStr: string = "";

        for (let pair of this.directGraph) {
            //console.log(pair[0] + "[" + pair[1].join(',') + "]");
            mapStr += pair[0] + "[" + pair[1].join(',') + "]";
        }

        return mapStr;
    }

    analyze() {

        for (let pair of this.allNodes) {
            //console.log('node:' + pair[0] );
            this.nodes.unshift({
                group: "nodes",
                data: {
                    id: pair[0]
                }
            });
        }

        for (let pair of this.directGraph) {
           // console.log(pair[0] + "[" + pair[1].join(',') + "]");
             for (let edgeNode of pair[1]) {

                var label = "";
                var style = "";

                //console.log(pair[0] + ' indexOf:' + pair[1].indexOf('~' + edgeNode) + ' edge: +~' + edgeNode);
                if ((edgeNode.substring(0, 1) == "~")||(pair[1].indexOf('~' + edgeNode) > -1)) {
                    edgeNode = edgeNode.replace(/[~]/gi, '')
                    label = "not";
                    style = "important";
                }
                if (pair[0] == edgeNode) {
                    style = "important";
                }
                this.edges.unshift({
                    group: "edges",
                    data: {
                        id: pair[0] + edgeNode,
                        source: pair[0],
                        target: edgeNode,
                        label: label
                    },
                    classes: style
                });
            }

        }
    }

    parseAtom(): string {
        var name = this.currentToken;
        if (!/^[A-Za-z_\d]+$/.test(name)) {
            //throw new SyntaxError('Bad atom name: ' + name);
            log.push("Bad atom name: " + name);
            this.errorParser = true;
            return null;
        }
        this.nextToken();
        return name;
    }


    parseTerm(): any {

        var name = this.parseAtom();

        if (/^[A-Z_][A-Za-z_\d]*$/.test(name)) {
            if (name === '_') {
                return new Variable('_');
            }
            // variable X in the same scope should point to the same object
            var variable = this.variableScope[name];
            if (!variable) {
                variable = this.variableScope[name] = new Variable(name);
            }
            return variable;
        }

        return new Constant(name);

    }

    parseLiteral(): Literal {

        //console.log("Parsing Literal ...");
        //console.log("this.currentToken:" + this.currentToken);

        var isPositiveLiteral = true;
        if ((this.currentToken === 'not') || (this.currentToken === '~')) {

            this.nextToken();
            isPositiveLiteral = false;
        }
        //console.log("this.currentToken:" + this.currentToken);
        var name = this.parseAtom();
        // var arguments: Arguments;

        if (this.currentToken === '(') {
            //console.log("this.currentToken:" + this.currentToken);
            this.nextToken(); // eat (
            //console.log("this.currentToken:" + this.currentToken);
            var listTerm = [];
            while (this.currentToken !== ')') {
                listTerm.push(this.parseTerm());
                if (this.currentToken !== ',' && this.currentToken !== ')') {
                    //throw new SyntaxError('Expected , or ) in Literal but got ' + this.currentToken);
                    log.push("Expected , or ) in Literal but got: " + this.currentToken);
                    this.errorParser = true;
                    return null;
                }
                if (this.currentToken === ',') {
                    this.nextToken(); // eat ,
                }
                //console.log("this.currentToken:" + this.currentToken);
            }
            this.nextToken(); // eat )
            // arguments = new Arguments(listTerm);
            //console.log("this.currentToken:" + this.currentToken);
        }

        //console.log("literal:" + name + listTerm + isPositiveLiteral);

        return new Literal(name, listTerm, isPositiveLiteral);

    }



    parseRule(): any {
        var head = this.parseLiteral();

        //console.log('head:'+head);

        if (this.currentToken === '.') { // Is a Fact with body TRUE
            this.nextToken(); // eat .
            // return new Rule(head, Literal.TRUE);
            if (head.isFactLiteral()) {
                var newFact = new Rule(head, []);
                newFact.isFact = true;
                //console.log('newFact:' + newFact);

                this.directGraph.set(head.relation, []); //check recursion for fix
                //this.isDerivatedRelation.set()
                //this.directGraph.set('t', ['test']);//check recursion for fix

                // console.log("head" + head + "head.relation:" + head.relation + " directgraph: " + JSON.stringify(this.directGraph.get('t')));

                return newFact;
            }
            console.log("Literal:" + head + " should be a Fact literal!.")
            log.push("Literal:" + head + " should be a Fact literal!.");
            this.errorParser = true;
            return null;
        }

        if (this.currentToken !== ':-') {
            //console.log(current);
            //throw new SyntaxError('Expected :- in rule but got ' + this.currentToken);
            log.push("Expected :- in rule but got  " + this.currentToken);
            this.errorParser = true;
            return null;


        }
        this.nextToken(); // eat :-

        var listLiteral = [];

        while (this.currentToken !== '.') {
            listLiteral.push(this.parseLiteral());
            if (this.currentToken !== ',' && this.currentToken !== '.') {
                //throw new SyntaxError('Expected , or ) in Literal but got ' + this.currentToken);
                log.push("Expected , or ) in Literal but got  " + this.currentToken);
                this.errorParser = true;
                return null;
            }
            if (this.currentToken === ',') {
                this.nextToken(); // eat ,
            }
        }
        this.nextToken(); // eat .
        //var body=[];
        var listLiteralNames;

        var newRule = new Rule(head, listLiteral);
        listLiteralNames = newRule.getLiteralNames();

        for (let relationInBody of listLiteralNames) {

            if (relationInBody.substring(0, 1) == "~") {
                relationInBody = relationInBody.replace(/[~]/gi, '')
            }
            this.allNodes.set(relationInBody,true);

        }

        if (this.directGraph.has(head.relation)) {
            var prevlistLiteralNames = this.directGraph.get(head.relation);
            listLiteralNames = listLiteralNames.concat(prevlistLiteralNames);
            //console.log("head" + head + "head.relation:" + head.relation + " directgraph: " + JSON.stringify(listLiteralNames));
        }
        this.directGraph.set(head.relation, listLiteralNames);
        this.allNodes.set(head.relation,true);

        //console.log("head" + head + "head.relation:" + head.relation + " directgraph: " + this.getDirectGraph() + "lenght:" + listLiteralNames.length);
        return newRule;
    }


    parseRules(): any {
        this.nextToken(); // start the tokens iterator
        var rules = [];
        while (!this.doneTokenList) {
            // each rule gets its own scope for variables
            this.variableScope = {};
            var parsedRule = this.parseRule();
            if (parsedRule) rules.push(parsedRule);
        }
        //console.log("RULES PARSED: \n\n" + rules.join('.\n') + '.');
        return rules;
    }

    parseQuery(): any {

        this.nextToken();
        this.variableScope = {};

        //console.log(parseLiteral());
        return this.parseLiteral();
    }
}"use strict";
export class AnswerLiteral extends Literal {

    adorment: string;

    constructor(relation, listTerm, isPositive, adorment) {
        super(relation, listTerm, isPositive);
        this.adorment = adorment;
    }

    toString = function() {

        if (this.isPositive)
            return 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
        else
            return " not " + 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
    };

}"use strict";
export class QueryLiteral extends Literal {


    adorment: string;

    constructor(relation, listTerm, adorment) {
        super(relation, listTerm, true);
        this.adorment = adorment;

    }


    toString = function() {

        if (this.adorment == "0") {

            return 'query_' + this.relation;
        }
        return 'query_' + this.relation + this.adorment + '(' + this.listTerm.join(',') + ')';
    };

}"use strict";
export class SIPS {

    adorment: string;
    rule: Rule;
    sipsRule: SipsRule;


    constructor(rule, adorment) {

        this.rule = rule;
        this.adorment = adorment;

    }

    optimalRule(): any {

        var queryTerm = new Map();

        for (var i = 0; i < this.adorment.length; i++) {
            //console.log("terms:" + this.rule.head.listTerm + " adorment:" + this.adorment);
            var index = this.adorment[i];
            var term = < Variable > this.rule.head.listTerm[ < any > index - 1];
            // console.log(" add term:" + term + " adorment:" + this.adorment);
            queryTerm.set(term.name, term);
        }
        //console.log("queryTerm:" + JSON.stringify([...queryTerm]));
        var newBody = [];
        var newNegativeBody = [];

        // console.log("processing rule:" + this.rule);

        for (let bodyliteral of this.rule.body) {

            var totalBind = 0;

            for (let term of bodyliteral.listTerm) {
                var variable = < Variable > term;
                if (queryTerm.has(variable.name)) {
                    totalBind++;

                    // console.log("variable.name:" + variable.name + " variable: " + variable);
                }
            }
            bodyliteral.totalBind = totalBind;
            if (bodyliteral.isPositive) {
                newBody.push(bodyliteral);
            } else
                newNegativeBody.push(bodyliteral);


        }
        var sortedSips: Literal[] = newBody.sort((n1, n2) => n2.totalBind - n1.totalBind);

        if (newNegativeBody.length > 0) {
            var sortedNegativeSips: Literal[] = newNegativeBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
            sortedSips = sortedSips.concat(sortedNegativeSips);

        }

        console.log("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        log.push("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);

        this.sipsRule = new SipsRule(this.rule.head, sortedSips, this.rule.head.relation + this.adorment);
        console.log("sipsrule:" + this.sipsRule);
        return sortedSips;

    };

    optimalRuleWithoutAdorment(directGraph): any { //adorment 0


        //console.log("queryTerm:" + JSON.stringify([...queryTerm]));
        var newBaseBody = [];
        var newDerivatedBody = [];
        var newNegativeBody = [];

        // console.log("processing rule:" + this.rule);

        for (let bodyliteral of this.rule.body) {

            bodyliteral.totalBind = bodyliteral.listTerm.length;

            if (bodyliteral.isPositive) {
                if (directGraph.has(bodyliteral.relation)) { //It is a derivated relation
                    newDerivatedBody.push(bodyliteral);
                } else //It is a base relation
                    newBaseBody.push(bodyliteral);
            } else
                newNegativeBody.push(bodyliteral);


        }
        var sortedSips: Literal[] = newBaseBody.sort((n1, n2) => n1.totalBind - n2.totalBind);

        if (newDerivatedBody.length > 0) {
            var sortedDerivatedSips: Literal[] = newDerivatedBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedDerivatedSips);

        }

        if (newNegativeBody.length > 0) {
            var sortedNegativeSips: Literal[] = newNegativeBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedNegativeSips);

        }

        console.log("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        log.push("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);

        this.sipsRule = new SipsRule(this.rule.head, sortedSips, this.rule.head.relation);
        console.log("sipsrule:" + this.sipsRule);
        return sortedSips;

    };

    toString = function() {
        return 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
    };

}"use strict";
export class MagicSet {

    ruleDb: Database;
    directGraph;
    answerList: Rule[];
    queryList: Rule[];
    sipsList: SipsRule[];

    constructor(ruleDb, directGraph) {
        this.ruleDb = ruleDb;
        this.directGraph = directGraph;
    }


    getCombinations(chars): any {
            var result = [];
            var f = function(prefix, chars) {
                for (var i = 0; i < chars.length; i++) {
                    result.push(prefix + chars[i]);
                    f(prefix + chars[i], chars.slice(i + 1));
                }
            }
            f('', chars);
            return result;
        }
        /*
        Usage:

        var combinations = getCombinations(["a", "b", "c", "d"]);
        Result:

        ["a", "ab", "abc", "abcd", "abd", "ac", "acd", "ad", "b", "bc", "bcd", "bd", "c", "cd", "d"] */


    generateMS(): any {

        this.answerList = [];
        this.queryList = [];
        this.sipsList = [];

        for (let rule of this.ruleDb.getRules()) {

            var listAdormentStr = "";

            for (var i = 1; i <= rule.head.listTerm.length; i++) {
                listAdormentStr += '' + i;
            }
            //console.log("adorment:" + listAdormentStr);

            //if (listAdormentStr.length > 1) {

            var listAdorment = this.getCombinations(listAdormentStr)
            listAdorment.push('0');

            //console.log("adormentCombinations:" + listAdorment);

            for (let adorment of listAdorment) {
                //console.log("1processing rule:" + rule);

                var unifiedVar = new Map();

                var sipsBody;

                var sipsObj = new SIPS(rule, adorment);

                if (adorment != '0') {

                    sipsBody = sipsObj.optimalRule();
                    this.sipsList.push(sipsObj.sipsRule);
                } else {
                    sipsBody = sipsObj.optimalRuleWithoutAdorment(this.directGraph);
                    this.sipsList.push(sipsObj.sipsRule);
                }

                var listTermQuery = [];

                //console.log("terms:" + rule.head.listTerm + " adorment:" + adorment);

                if (adorment != '0')
                    for (var i = 0; i < adorment.length; i++) {

                        var term = < Variable > rule.head.listTerm[( < number > adorment[i]) - 1];
                        listTermQuery.push(term);
                        unifiedVar.set(term.name, true);
                        //console.log("terms:" + rule.head.listTerm + " adorment:" + adorment);
                    }
                    //console.log("listTermQuery:" + listTermQuery);

                var answerHead = new AnswerLiteral(rule.head.relation, rule.head.listTerm, rule.head.isPositive, adorment);
                var querySeed = new QueryLiteral(rule.head.relation, listTermQuery, adorment);

                var listLiteral = [];

                listLiteral.push(querySeed);

                for (let bodyliteral of sipsBody) {

                    var newLiteral;
                    var queryLiteral;

                    //console.log("  sipsBody:" + sipsBody + " bodyliteral:" + bodyliteral );

                    if (this.directGraph.has(bodyliteral.relation)) { //It is a derivated relation

                        var listTermQueryLiteral = []
                        var adormentQuery = "";

                       //  console.log("unifiedVarmap:" + JSON.stringify([...unifiedVar]));

                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {

                            var term = < Variable > bodyliteral.listTerm[i];

                            if (unifiedVar.has(term.name)) {
                                listTermQueryLiteral.push(term);
                                adormentQuery += "" + (i + 1);

                           //      console.log("  bodyliteral.listTerm:" + bodyliteral.listTerm + " term:" + term + " adormentQuery:" + adormentQuery);
                            }
                        }

                        newLiteral = new AnswerLiteral(bodyliteral.relation, bodyliteral.listTerm, bodyliteral.isPositive, adormentQuery);
                        queryLiteral = new QueryLiteral(bodyliteral.relation, listTermQueryLiteral, adormentQuery);

                       // console.log("  AnswerLiteral:" + newLiteral + "  Positive:" + newLiteral.isPositive + " queryLiteral:" + queryLiteral);


                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {

                            var term = < Variable > bodyliteral.listTerm[i];
                            //console.log("aca- terms:" + bodyliteral.listTerm + " term.name added:" + term.name + " adorment:" + adorment);
                            unifiedVar.set(term.name, true);
                        }

                        var newlistLiteral = listLiteral.slice();

                        var MSQueryRule = new Rule(queryLiteral, newlistLiteral);

                        //if (!adorment0Added) {
                        //    var querySeed0 = new QueryLiteral(rule.head.relation, [], '0');

                        //}

                        this.queryList.push(MSQueryRule);

                        //console.log("MSQueryRule:" + MSQueryRule);


                    } else { //base relation
                        newLiteral = bodyliteral;

                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {

                            var term = < Variable > bodyliteral.listTerm[i];
                            //console.log("aca2-terms:" + bodyliteral.listTerm + " term.name added:" + term.name + " adorment:" + adorment);
                            unifiedVar.set(term.name, true);
                        }


                    }
                    listLiteral.push(newLiteral);
                }

                var MSAnswerRule = new Rule(answerHead, listLiteral);
                this.answerList.push(MSAnswerRule);
                //console.log("MSRule:" + MSAnswerRule);

            }


        }


    }

    toString = function() {

        return "MAGIC RULES:\n\n" + this.queryList.join('.\n') + '.' + '\n\n' + this.answerList.join('.\n') + '.';
    };


    SipsRulestoString = function() {
        var str = "";

        console.log("SipsRulestoString:" + this.sipsList);

        for (var i = 0; i < this.sipsList.length; i++) {

            str += this.sipsList[i].toStringWithAdorment() + "\n";
        }

        return str;
    };

}}