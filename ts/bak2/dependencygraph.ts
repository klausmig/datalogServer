"use strict";

class DependencyGraph {

    name: string;
    directedGraph;
    invertDirectedGraph;

    layerNode;
    maxLayer;
    allNodes;

    nodes: any;
    edges: any;

    constructor() {

        this.directedGraph = new Map();
        this.invertDirectedGraph = new Map();
        this.layerNode = new Map();
        this.allNodes = new Map();

        this.nodes = [];
        this.edges = [];

    }

    getDirectGraph() {

        var mapStr: string = "";

        for (let pair of this.directedGraph) {
            //console.log(pair[0] + "[" + pair[1].join(',') + "]");
            mapStr += pair[0] + "[" + pair[1].join(',') + "]";
        }

        return mapStr;
    }

    fixLayers(changedLayer, deep) {
        //if (deep== 2) return;
        for (let relationLayer of changedLayer) {

            var listRelation = this.invertDirectedGraph.get(relationLayer);
            var numLayer = this.layerNode.get(relationLayer);

            console.log('L' + numLayer + 'root:' + relationLayer + ' lits:' + listRelation);

            if (listRelation)
                for (let relationBody of listRelation) {
                    if (this.layerNode.get(relationBody) <= numLayer) {
                        if (this.maxLayer < numLayer + 1)
                            this.maxLayer = numLayer + 1;
                        this.layerNode.set(relationBody, numLayer + 1);
                        //this.fixLayers([relationBody], ++deep);
                    }
                }

        }

        return;


    }
    calculateInvertGraph() {

        for (let pair of this.directedGraph) {

            var node = pair[0];
            var listBody = pair[1];

            for (let edgeNode of listBody) {

                if ((edgeNode.substring(0, 1) == "~") || (listBody.indexOf('~' + edgeNode) > -1)) {
                    edgeNode = edgeNode.replace(/[~]/gi, '')
                }

                if (this.invertDirectedGraph.has(edgeNode)) {
                    var listRelation = this.invertDirectedGraph.get(edgeNode);
                    //console.log([...this.invertDirectedGraph]);
                    if (edgeNode != node) {
                        listRelation.push(node);
                        this.invertDirectedGraph.set(edgeNode, listRelation);
                    }
                } else
                if (edgeNode != node) {
                    this.invertDirectedGraph.set(edgeNode, [node]);
                }
            }

        }
        //console.log('\n\ninvertDirectedGraph:\n');
        //for (let pair of this.invertDirectedGraph) {
        //    console.log('relation:' + pair[0] + ' [' + pair[1].join(',') + ']');

        //}
    }

    calculateLayer(node, listBody) {

        var safeLayer = true;
        var maxLayer = 0;

        for (let edgeNode of listBody) {

            if ((edgeNode.substring(0, 1) == "~") || (listBody.indexOf('~' + edgeNode) > -1)) {
                edgeNode = edgeNode.replace(/[~]/gi, '')
            }
            if (node != edgeNode)
                if (this.layerNode.has(edgeNode)) {
                    if (this.layerNode.get(edgeNode) > maxLayer)
                        maxLayer = this.layerNode.get(edgeNode);
                } else {
                    //if (edgeNode != pair[0])
                    //    if (maxLayer == 0) {
                    //        this.layerNode.set(edgeNode, ++maxLayer);
                    //    } else
                    //        this.layerNode.set(edgeNode, maxLayer);
                    safeLayer = false;
                }
        }

        if (safeLayer)
            return maxLayer + 1;
        else
            return -1;
    }

    stratified() {

        this.calculateInvertGraph();

        for (let pair of this.allNodes) {
            //console.log('node:' + pair[0] );

            if (!this.directedGraph.has(pair[0])) { // base relations are in layer 0
                this.layerNode.set(pair[0], 0);
            }
        }

        this.maxLayer = 0;

        var withLayer = [];

        for (let pair of this.directedGraph) {
            //           console.log(pair[0] + "[" + pair[1].join(',') + "]");
            var maxLayer = 0;
            var node = pair[0];

            var numLayer = this.calculateLayer(node, pair[1]);

            if (numLayer > 0) {
                this.layerNode.set(node, numLayer);
                withLayer.push(node);

                if (this.maxLayer < numLayer)
                    this.maxLayer = numLayer;
            }

            //console.log('node:' + node + " layer:" + numLayer);


            //if (this.layerNode.has(pair[0])) {

            //    //console.log("has node:" + pair[0]);

            //    if (this.layerNode.get(pair[0]) < maxLayer + 1) {
            //        changedLayer.push(pair[0]);
            //        this.layerNode.set(pair[0], maxLayer + 1)
            //        if (this.maxLayer < maxLayer + 1)
            //            this.maxLayer = maxLayer + 1;
            //    }
            //} else {
            //    this.layerNode.set(pair[0], maxLayer + 1);
            //    if (this.maxLayer < maxLayer + 1)
            //        this.maxLayer = maxLayer + 1;
            //}


        }


        //console.log('\n\witlayer:\n');
        //for (let pair of withLayer) {
        //    console.log('relation:' + pair);

        //}

        var i = 0;
        while (withLayer.length > 0) {
            i++;
            var nodeWithLayer = withLayer.shift();

            if (this.invertDirectedGraph.has(nodeWithLayer)) {
                var listBody = this.invertDirectedGraph.get(nodeWithLayer);

                for (let edgeNode of listBody) {

                    if (!this.layerNode.has(edgeNode)) {
                        var numLayer = this.calculateLayer(edgeNode, this.directedGraph.get(edgeNode));
                        if (numLayer > 0) {
                            this.layerNode.set(edgeNode, numLayer);
                            withLayer.push(edgeNode);
                            if (this.maxLayer < numLayer)
                                this.maxLayer = numLayer;
                        }
                    }
                }
            }

            if (i == 100) break;
        }

        //console.log('\n\layerNode:\n');
        //for (let pair of this.layerNode) {
        //    console.log('relation:' + pair[0] + ' layer:' + pair[1]);

        //}

    }

    analyze() {


        this.stratified();

        for (let pair of this.directedGraph) {
            //           console.log(pair[0] + "[" + pair[1].join(',') + "]");

            for (let edgeNode of pair[1]) {

                var label = "";
                var style = "";

                if ((edgeNode.substring(0, 1) == "~") || (pair[1].indexOf('~' + edgeNode) > -1)) {
                    edgeNode = edgeNode.replace(/[~]/gi, '')
                    label = "not";
                    style = "critical";
                }


                //console.log(pair[0] + ' indexOf:' + pair[1].indexOf('~' + edgeNode) + ' edge: +~' + edgeNode);
                if (pair[0] == edgeNode) {
                    style = "important";
                }
                this.edges.unshift({
                    group: "edges",
                    data: {
                        id: pair[0] + edgeNode,
                        source: pair[0],
                        target: edgeNode,
                        label: label
                    },
                    classes: style
                });
            }


            //    console.log('this.maxLayer:' + this.maxLayer)
        }

        //console.log('changedLayer:' + changedLayer + ' invertDirectedGraph:' + [... this.invertDirectedGraph]);


        // this.fixLayers(changedLayer,1);


        for (let pair of this.allNodes) {
            //console.log('node:' + pair[0] );
            this.nodes.unshift({
                group: "nodes",
                data: {
                    id: pair[0]
                },
                classes: "l" + this.layerNode.get(pair[0])
            });

        }

        // console.log("layer:[" + [...this.layerNode] + "]");
    }


    toString = function() {
        return this.name;
    };
}