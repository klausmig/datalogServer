"use strict";

class Literal {
    relation: string;
    listTerm: Term[];
    totalBind: number;
    isPositive: boolean = true;
    isGround: boolean = false;

    constructor(relation, listTerm, isPositive) {
        this.relation = relation;
        this.listTerm = listTerm || [];
        this.isPositive = isPositive;
    }

    isFactLiteral(): boolean {
        for (var term of this.listTerm) {
            //console.log("validation:" + term + " type:" + term.type);
            if (term.type == "Variable") {

                this.isGround = false;
                return false;
            }
        }
        this.isGround = true;
        return true;
    }

    match(otherLiteral): Map < Variable, Constant > {

        var matchObj = new Match();

        if (otherLiteral instanceof Literal) {
            if ((this.relation !== otherLiteral.relation) || (this.listTerm.length !== otherLiteral.listTerm.length)) {
                return null;
            }
            //console.log("actualliteral:" + this);
            //if (this.isGround) {
            //console.log("Actualliteral:" + this + " otherLiteral:" + otherLiteral);
            for (var i = 0; i < otherLiteral.listTerm.length; i++) {

                var term = this.listTerm[i];
                var otherTerm = otherLiteral.listTerm[i];
               // console.log('bindings:' + JSON.stringify([...matchObj.bindingMap]));
                if (!term.isConstant && otherTerm.isConstant) {
                    matchObj.bindingMap.set( < Variable > term, < Constant > otherTerm); // binding variable and constant
                } else
                if (term.isConstant && !otherTerm.isConstant) {
                    matchObj.bindingMap.set( < Variable > otherTerm, < Constant > term); // binding variable and constant
                } else
                if (term.isConstant && otherTerm.isConstant) {
                    if (( < Constant > term).value !== ( < Constant > otherTerm).value) { // compare constant value must be equal for match
                        //console.log("turn nulldiff headterm:" + this.listTerm[i] + " goalterm:" + otherLiteral.listTerm[i]);
                        return null;
                    } else {
                        matchObj.bindingMap.set( < Constant > term, < Constant > otherTerm);
                    }
                }
            }
            //console.log('Bindings:' + JSON.stringify([...matchObj.bindingMap]));
            return matchObj.bindingMap;
        }

    };

    substitute = function(bindings) {

        var argSubstitute = this.listTerm.map(function(term) {

            if (term instanceof Variable) {
                //console.log('Termtosubstitute:' + term + ' bindings:' + JSON.stringify([...bindings]));
                return term.substitute(bindings);
            } else
                return term; //It is a Constant
        });

        var newLiteralSustutute = new Literal(this.relation, argSubstitute, this.isPositive);

        return newLiteralSustutute;
    };

    query = function*(database) {
        yield * database.query(this);
    };

    toString = function() {
        if (this.listTerm.length === 0) {
            return this.relation;
        }

        if (this.isPositive)
            return this.relation + '(' + this.listTerm.join(',') + ')';
        else
            return " not " + this.relation + '(' + this.listTerm.join(',') + ')';
        //if (this.totalBind)
        //    return this.relation + '(' + this.listTerm.join(',') + ')' + this.totalBind;
        //else

    };

}