//"use strict";

class MagicRule {

	rule:Rule;
	queryRules:Rule[];
    answerRules: Rule[];
    sipsRules: Rule[];

    constructor(rule, queryRules, answerRules, sipsRules) {
		this.rule = rule;
		this.queryRules = queryRules;
        this.answerRules = answerRules;
        this.sipsRules = sipsRules;
	}

	toString = function() {
        return this.rule + "\n [" + this.queryRules.join("\n") + "]" + "\n [" + this.answerRules.join("\n") + "]\n";
	};


}