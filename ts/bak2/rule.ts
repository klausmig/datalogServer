"use strict";
class Rule {

    head: Literal;
    body: Literal[];
    isFact: boolean = false;

    constructor(head, body) {
        this.head = head;
        this.body = body;
    }

    getLiteralNames() {
        var literalNameList = []
        for (var item of this.body) {

            var relationName = item.relation;
            if (!item.isPositive)
                relationName = '~' + item.relation;

            literalNameList.unshift(relationName);
        }
        return literalNameList;
    }


    substituteBody(bindings) {
        // console.log('bodybindings:' + JSON.stringify([...bindings]));
        return this.body.map(function(literal) {
            return literal.substitute(bindings);
        });
    };

    queryBody = function*(database) {
        var self = this;

        function* solutions(index, bindings) {
            var literal = self.body[index];
            if (!literal) {
                //yield self.substituteBody(bindings);
                yield self.head.substitute(bindings);
            } else {
                //console.log("quering literal:" + literal.substitute(bindings) + "lit:" + literal);
                if (!literal.isPositive) { //negative Literal
                    var positiveMatch = false;
                    for (var item of database.query(literal.substitute(bindings))) { //check all items from DB matched with the literal
                        var newBinding = literal.match(item);
                        if (newBinding.size > 0) { //no matching for next term in body
                            positiveMatch = true;
                            break;
                        }
                    }

                    if (!positiveMatch)
                        yield * solutions(index + 1, bindings);
                } else //Positive Literal
                    for (var item of database.query(literal.substitute(bindings))) {
                    var newBinding = literal.match(item);
                    if (newBinding.size > 0) { //no matching for next term in body
                        var unified = new Map([...bindings, ...literal.match(item)]); // concatenate the bindings for every term in the body
                        //console.log('unifiedBinding:' + JSON.stringify([...unified]));
                        if (unified) {
                            yield * solutions(index + 1, unified);
                        }
                    }
                }
            }
        }
        yield * solutions(0, new Map);

    }


    toString = function() {

        if (this.isFact)
            return this.head.toString();
        return this.head + ' :- ' + this.body;
    };

}