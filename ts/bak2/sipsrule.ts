"use strict";

class SipsRule extends Rule {

	adorment: string;

	constructor(head, body, adorment) {
		super(head, body);
		this.adorment = adorment;
	}

	toStringWithAdorment = function() {

		return this.adorment + ": " + this.head + ' :- ' + this.body;

	};

}