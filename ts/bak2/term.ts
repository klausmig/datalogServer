//"use strict";

class Term {

	type: string;
	isConstant: boolean = false;

	constructor(type) {
		this.type = type;
	}

	toString = function() {
		return this.type;
	};


}