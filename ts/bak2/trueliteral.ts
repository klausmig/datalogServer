﻿"use strict";
class TrueLiteral extends Literal {


	constructor() {
		super('true', null, true);
	}

	substitute = function() {
		return this;
	};

	query = function*() {
		yield this;
	};
}