"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Term {
    constructor(type) {
        this.isConstant = false;
        this.toString = function () {
            return this.type;
        };
        this.type = type;
    }
}
exports.Term = Term;
"use strict";
class Constant extends Term {
    constructor(value) {
        super("Constant");
        this.toString = function () {
            return this.value;
        };
        this.value = value;
        this.isConstant = true;
    }
}
exports.Constant = Constant;
"use strict";
class Variable extends Term {
    constructor(name) {
        super('Variable');
        this.match = function (other) {
            var bindings = new Map;
            if (this !== other) {
                bindings.set(this, other);
            }
            return bindings;
        };
        this.substitute = function (bindings) {
            var value = bindings.get(this);
            if (value) {
                return value;
            }
            return this;
        };
        this.toString = function () {
            return this.name;
        };
        this.name = name;
    }
}
exports.Variable = Variable;
"use strict";
class Match {
    constructor() {
        this.isMatched = false;
        this.bindingMap = new Map();
    }
    ;
    getLiteralNames() {
        var literalNameList = [];
        return literalNameList;
    }
}
exports.Match = Match;
"use strict";
class Literal {
    constructor(relation, listTerm, isPositive) {
        this.isPositive = true;
        this.isGround = false;
        this.substitute = function (bindings) {
            var argSubstitute = this.listTerm.map(function (term) {
                if (term instanceof Variable) {
                    return term.substitute(bindings);
                }
                else
                    return term;
            });
            var newLiteralSustutute = new Literal(this.relation, argSubstitute, this.isPositive);
            return newLiteralSustutute;
        };
        this.query = function* (database) {
            yield* database.query(this);
        };
        this.toString = function () {
            if (this.listTerm.length === 0) {
                return this.relation;
            }
            if (this.isPositive)
                return this.relation + '(' + this.listTerm.join(',') + ')';
            else
                return " not " + this.relation + '(' + this.listTerm.join(',') + ')';
        };
        this.relation = relation;
        this.listTerm = listTerm || [];
        this.isPositive = isPositive;
    }
    isFactLiteral() {
        for (var term of this.listTerm) {
            if (term.type == "Variable") {
                this.isGround = false;
                return false;
            }
        }
        this.isGround = true;
        return true;
    }
    match(otherLiteral) {
        var matchObj = new Match();
        if (otherLiteral instanceof Literal) {
            if ((this.relation !== otherLiteral.relation) || (this.listTerm.length !== otherLiteral.listTerm.length)) {
                return null;
            }
            for (var i = 0; i < otherLiteral.listTerm.length; i++) {
                var term = this.listTerm[i];
                var otherTerm = otherLiteral.listTerm[i];
                if (!term.isConstant && otherTerm.isConstant) {
                    matchObj.bindingMap.set(term, otherTerm);
                }
                else if (term.isConstant && !otherTerm.isConstant) {
                    matchObj.bindingMap.set(otherTerm, term);
                }
                else if (term.isConstant && otherTerm.isConstant) {
                    if (term.value !== otherTerm.value) {
                        return null;
                    }
                    else {
                        matchObj.bindingMap.set(term, otherTerm);
                    }
                }
            }
            return matchObj.bindingMap;
        }
    }
    ;
}
exports.Literal = Literal;
"use strict";
class Rule {
    constructor(head, body) {
        this.isFact = false;
        this.queryBody = function* (database) {
            var self = this;
            function* solutions(index, bindings) {
                var literal = self.body[index];
                if (!literal) {
                    yield self.head.substitute(bindings);
                }
                else {
                    if (!literal.isPositive) {
                        var positiveMatch = false;
                        for (var item of database.query(literal.substitute(bindings))) {
                            var newBinding = literal.match(item);
                            if (newBinding.size > 0) {
                                positiveMatch = true;
                                break;
                            }
                        }
                        if (!positiveMatch)
                            yield* solutions(index + 1, bindings);
                    }
                    else
                        for (var item of database.query(literal.substitute(bindings))) {
                            var newBinding = literal.match(item);
                            if (newBinding.size > 0) {
                                var unified = new Map([...bindings, ...literal.match(item)]);
                                if (unified) {
                                    yield* solutions(index + 1, unified);
                                }
                            }
                        }
                }
            }
            yield* solutions(0, new Map);
        };
        this.toString = function () {
            if (this.isFact)
                return this.head.toString();
            return this.head + ' :- ' + this.body;
        };
        this.head = head;
        this.body = body;
    }
    getLiteralNames() {
        var literalNameList = [];
        for (var item of this.body) {
            var relationName = item.relation;
            if (!item.isPositive)
                relationName = '~' + item.relation;
            literalNameList.unshift(relationName);
        }
        return literalNameList;
    }
    substituteBody(bindings) {
        return this.body.map(function (literal) {
            return literal.substitute(bindings);
        });
    }
    ;
}
exports.Rule = Rule;
"use strict";
class SipsRule extends Rule {
    constructor(head, body, adorment) {
        super(head, body);
        this.toStringWithAdorment = function () {
            return this.adorment + ": " + this.head + ' :- ' + this.body;
        };
        this.adorment = adorment;
    }
}
exports.SipsRule = SipsRule;
"use strict";
class Database {
    constructor(rules) {
        this.queryDB = function (goal) {
            var results = [];
            for (var item of this.query(goal)) {
                results.push(item + "");
            }
            var uniqueResult = Array.from(new Set(results));
            return (uniqueResult);
        };
        this.query = function* (goal) {
            for (var i = 0, rule; rule = this.rules[i]; i++) {
                var match = rule.head.match(goal);
                if (match) {
                    var head = rule.head.substitute(match);
                    if (!rule.isFact) {
                        var body = rule.substituteBody(match);
                        var newQueryRule = new Rule(head, body);
                        for (var item of newQueryRule.queryBody(this)) {
                            yield item;
                        }
                    }
                    else
                        yield head;
                }
            }
        };
        this.rules = rules;
    }
    toString() {
        return this.rules.join('.\n') + '.';
    }
    toHtml() {
        return this.rules.join('.<br/>') + '.';
    }
    getRules() {
        return this.rules;
    }
}
exports.Database = Database;
var log = [];
exports.log = log;
function* lexer(text) {
    var tokenRegexp = /[A-Za-z_\d]+|:\-|[()\.,~]/g;
    var match;
    while ((match = tokenRegexp.exec(text)) !== null) {
        yield match[0];
    }
}
exports.lexer = lexer;
"use strict";
class DependencyGraph {
    constructor() {
        this.toString = function () {
            return this.name;
        };
        this.directedGraph = new Map();
        this.invertDirectedGraph = new Map();
        this.layerNode = new Map();
        this.allNodes = new Map();
        this.nodes = [];
        this.edges = [];
    }
    getDirectGraph() {
        var mapStr = "";
        for (let pair of this.directedGraph) {
            mapStr += pair[0] + "[" + pair[1].join(',') + "]";
        }
        return mapStr;
    }
    fixLayers(changedLayer, deep) {
        for (let relationLayer of changedLayer) {
            var listRelation = this.invertDirectedGraph.get(relationLayer);
            var numLayer = this.layerNode.get(relationLayer);
            console.log('L' + numLayer + 'root:' + relationLayer + ' lists:' + listRelation);
            if (listRelation)
                for (let relationBody of listRelation) {
                    if (this.layerNode.get(relationBody) <= numLayer) {
                        if (this.maxLayer < numLayer + 1)
                            this.maxLayer = numLayer + 1;
                        this.layerNode.set(relationBody, numLayer + 1);
                    }
                }
        }
        return;
    }
    calculateInvertGraph() {
        for (let pair of this.directedGraph) {
            var node = pair[0];
            var listBody = pair[1];
            for (let edgeNode of listBody) {
                if ((edgeNode.substring(0, 1) == "~") || (listBody.indexOf('~' + edgeNode) > -1)) {
                    edgeNode = edgeNode.replace(/[~]/gi, '');
                }
                if (this.invertDirectedGraph.has(edgeNode)) {
                    var listRelation = this.invertDirectedGraph.get(edgeNode);
                    if (edgeNode != node) {
                        listRelation.push(node);
                        this.invertDirectedGraph.set(edgeNode, listRelation);
                    }
                }
                else if (edgeNode != node) {
                    this.invertDirectedGraph.set(edgeNode, [node]);
                }
            }
        }
    }
    calculateLayer(node, listBody) {
        var safeLayer = true;
        var maxLayer = 0;
        for (let edgeNode of listBody) {
            if ((edgeNode.substring(0, 1) == "~") || (listBody.indexOf('~' + edgeNode) > -1)) {
                edgeNode = edgeNode.replace(/[~]/gi, '');
            }
            if (node != edgeNode)
                if (this.layerNode.has(edgeNode)) {
                    if (this.layerNode.get(edgeNode) > maxLayer)
                        maxLayer = this.layerNode.get(edgeNode);
                }
                else {
                    safeLayer = false;
                }
        }
        if (safeLayer)
            return maxLayer + 1;
        else
            return -1;
    }
    stratified() {
        this.calculateInvertGraph();
        for (let pair of this.allNodes) {
            if (!this.directedGraph.has(pair[0])) {
                this.layerNode.set(pair[0], 0);
            }
        }
        this.maxLayer = 0;
        var withLayer = [];
        for (let pair of this.directedGraph) {
            var maxLayer = 0;
            var node = pair[0];
            var numLayer = this.calculateLayer(node, pair[1]);
            if (numLayer > 0) {
                this.layerNode.set(node, numLayer);
                withLayer.push(node);
                if (this.maxLayer < numLayer)
                    this.maxLayer = numLayer;
            }
        }
        var i = 0;
        while (withLayer.length > 0) {
            i++;
            var nodeWithLayer = withLayer.shift();
            if (this.invertDirectedGraph.has(nodeWithLayer)) {
                var listBody = this.invertDirectedGraph.get(nodeWithLayer);
                for (let edgeNode of listBody) {
                    if (!this.layerNode.has(edgeNode)) {
                        var numLayer = this.calculateLayer(edgeNode, this.directedGraph.get(edgeNode));
                        if (numLayer > 0) {
                            this.layerNode.set(edgeNode, numLayer);
                            withLayer.push(edgeNode);
                            if (this.maxLayer < numLayer)
                                this.maxLayer = numLayer;
                        }
                    }
                }
            }
            if (i == 100)
                break;
        }
    }
    analyze() {
        this.stratified();
        for (let pair of this.directedGraph) {
            for (let edgeNode of pair[1]) {
                var label = "";
                var style = "";
                if ((edgeNode.substring(0, 1) == "~") || (pair[1].indexOf('~' + edgeNode) > -1)) {
                    edgeNode = edgeNode.replace(/[~]/gi, '');
                    label = "not";
                    style = "critical";
                }
                if (pair[0] == edgeNode) {
                    style = "important";
                }
                this.edges.unshift({
                    group: "edges",
                    data: {
                        id: pair[0] + edgeNode,
                        source: pair[0],
                        target: edgeNode,
                        label: label
                    },
                    classes: style
                });
            }
        }
        for (let pair of this.allNodes) {
            this.nodes.unshift({
                group: "nodes",
                data: {
                    id: pair[0]
                },
                classes: "l" + this.layerNode.get(pair[0])
            });
        }
    }
}
exports.DependencyGraph = DependencyGraph;
"use strict";
class Parser {
    constructor(tokens) {
        this.errorParser = false;
        this.errorMsg = "";
        this.tokens = tokens;
        this.dg = new DependencyGraph();
        this.errorMsg = '';
    }
    nextToken() {
        var nextTokenList = this.tokens.next();
        this.currentToken = nextTokenList.value;
        this.doneTokenList = nextTokenList.done;
    }
    parseAtom() {
        var name = this.currentToken;
        if (!/^[A-Za-z_\d]+$/.test(name)) {
            log.push("Bad atom name: " + name);
            this.errorParser = true;
            return null;
        }
        this.nextToken();
        return name;
    }
    parseTerm() {
        var name = this.parseAtom();
        if (/^[A-Z_][A-Za-z_\d]*$/.test(name)) {
            if (name === '_') {
                return new Variable('_');
            }
            var variable = this.variableScope[name];
            if (!variable) {
                variable = this.variableScope[name] = new Variable(name);
            }
            return variable;
        }
        return new Constant(name);
    }
    parseLiteral() {
        var isPositiveLiteral = true;
        if ((this.currentToken === 'not') || (this.currentToken === '~')) {
            this.nextToken();
            isPositiveLiteral = false;
        }
        var name = this.parseAtom();
        if (this.currentToken === '(') {
            this.nextToken();
            var listTerm = [];
            while (this.currentToken !== ')') {
                listTerm.push(this.parseTerm());
                if (this.currentToken !== ',' && this.currentToken !== ')') {
                    log.push("Expected , or ) in Literal but got: " + this.currentToken);
                    this.errorParser = true;
                    return null;
                }
                if (this.currentToken === ',') {
                    this.nextToken();
                }
            }
            this.nextToken();
        }
        return new Literal(name, listTerm, isPositiveLiteral);
    }
    parseRule() {
        var head = this.parseLiteral();
        if (this.currentToken === '.') {
            this.nextToken();
            if (head.isFactLiteral()) {
                var newFact = new Rule(head, []);
                newFact.isFact = true;
                this.dg.directedGraph.set(head.relation, []);
                return newFact;
            }
            console.log("Literal:" + head + " should be a Fact literal!.");
            log.push("Literal:" + head + " should be a Fact literal!.");
            this.errorParser = true;
            return null;
        }
        if (this.currentToken !== ':-') {
            log.push("Expected :- in rule but got  " + this.currentToken);
            this.errorParser = true;
            return null;
        }
        this.nextToken();
        var listLiteral = [];
        while (this.currentToken !== '.') {
            listLiteral.push(this.parseLiteral());
            if (this.currentToken !== ',' && this.currentToken !== '.') {
                log.push("Expected , or ) in Literal but got  " + this.currentToken);
                this.errorParser = true;
                return null;
            }
            if (this.currentToken === ',') {
                this.nextToken();
            }
        }
        this.nextToken();
        var listLiteralNames;
        var newRule = new Rule(head, listLiteral);
        listLiteralNames = newRule.getLiteralNames();
        for (let relationInBody of listLiteralNames) {
            if (relationInBody.substring(0, 1) == "~") {
                relationInBody = relationInBody.replace(/[~]/gi, '');
            }
            this.dg.allNodes.set(relationInBody, true);
        }
        if (this.dg.directedGraph.has(head.relation)) {
            var prevlistLiteralNames = this.dg.directedGraph.get(head.relation);
            listLiteralNames = listLiteralNames.concat(prevlistLiteralNames);
        }
        this.dg.directedGraph.set(head.relation, listLiteralNames);
        this.dg.allNodes.set(head.relation, true);
        return newRule;
    }
    parseRules() {
        this.nextToken();
        var rules = [];
        while (!this.doneTokenList) {
            this.variableScope = {};
            var parsedRule = this.parseRule();
            if (parsedRule)
                rules.push(parsedRule);
        }
        return rules;
    }
    parseQuery() {
        this.nextToken();
        this.variableScope = {};
        return this.parseLiteral();
    }
}
exports.Parser = Parser;
"use strict";
class AnswerLiteral extends Literal {
    constructor(relation, listTerm, isPositive, adorment) {
        super(relation, listTerm, isPositive);
        this.toString = function () {
            if (this.isPositive)
                return 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
            else
                return " not " + 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
        };
        this.adorment = adorment;
    }
}
exports.AnswerLiteral = AnswerLiteral;
"use strict";
class QueryLiteral extends Literal {
    constructor(relation, listTerm, adorment) {
        super(relation, listTerm, true);
        this.toString = function () {
            if (this.adorment == "0") {
                return 'query_' + this.relation;
            }
            return 'query_' + this.relation + this.adorment + '(' + this.listTerm.join(',') + ')';
        };
        this.adorment = adorment;
    }
}
exports.QueryLiteral = QueryLiteral;
"use strict";
class SIPS {
    constructor(rule, adorment) {
        this.toString = function () {
            return 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
        };
        this.rule = rule;
        this.adorment = adorment;
    }
    optimalRule() {
        var queryTerm = new Map();
        for (var i = 0; i < this.adorment.length; i++) {
            var index = this.adorment[i];
            var term = this.rule.head.listTerm[index - 1];
            queryTerm.set(term.name, term);
        }
        var newBody = [];
        var newNegativeBody = [];
        var unboundBody = [];
        var unboundNegativeBody = [];
        for (let bodyliteral of this.rule.body) {
            var totalBind = 0;
            for (let term of bodyliteral.listTerm) {
                var variable = term;
                if (queryTerm.has(variable.name)) {
                    totalBind++;
                }
            }
            bodyliteral.totalBind = totalBind;
            if (totalBind == 0)
                if (bodyliteral.isPositive) {
                    unboundBody.push(bodyliteral);
                }
                else
                    unboundNegativeBody.push(bodyliteral);
            else if (bodyliteral.isPositive) {
                newBody.push(bodyliteral);
            }
            else
                newNegativeBody.push(bodyliteral);
        }
        var sortedSips = newBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
        if (unboundBody.length > 0) {
            var unifiedTerm = new Map();
            for (let boundBodyliteral of sortedSips) {
                for (let term of boundBodyliteral.listTerm) {
                    var variable = term;
                    unifiedTerm.set(variable.name, true);
                }
            }
            var newBoundBody = [];
            for (let unboundBodyliteral of unboundBody) {
                var totalBind = 0;
                for (let term of unboundBodyliteral.listTerm) {
                    var variable = term;
                    if (unifiedTerm.has(variable.name)) {
                        totalBind++;
                    }
                }
                unboundBodyliteral.totalBind = totalBind;
                newBoundBody.push(unboundBodyliteral);
            }
            var sortedUnboundSips = newBoundBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
            sortedSips = sortedSips.concat(sortedUnboundSips);
        }
        if (newNegativeBody.length > 0) {
            var sortedNegativeSips = newNegativeBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
            sortedSips = sortedSips.concat(sortedNegativeSips);
        }
        if (unboundNegativeBody.length > 0) {
            var sortedNegativeSips = unboundNegativeBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
            sortedSips = sortedSips.concat(sortedNegativeSips);
        }
        log.push("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        this.sipsRule = new SipsRule(this.rule.head, sortedSips, this.rule.head.relation + this.adorment);
        return sortedSips;
    }
    ;
    optimalRuleWithoutAdorment(directGraph) {
        var newBaseBody = [];
        var newDerivatedBody = [];
        var newNegativeBody = [];
        for (let bodyliteral of this.rule.body) {
            bodyliteral.totalBind = bodyliteral.listTerm.length;
            if (bodyliteral.isPositive) {
                if (directGraph.has(bodyliteral.relation)) {
                    newDerivatedBody.push(bodyliteral);
                }
                else
                    newBaseBody.push(bodyliteral);
            }
            else
                newNegativeBody.push(bodyliteral);
        }
        var sortedSips = newBaseBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
        if (newDerivatedBody.length > 0) {
            var sortedDerivatedSips = newDerivatedBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedDerivatedSips);
        }
        if (newNegativeBody.length > 0) {
            var sortedNegativeSips = newNegativeBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedNegativeSips);
        }
        log.push("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        this.sipsRule = new SipsRule(this.rule.head, sortedSips, this.rule.head.relation);
        return sortedSips;
    }
    ;
}
exports.SIPS = SIPS;
class MagicRule {
    constructor(rule, queryRules, answerRules, sipsRules) {
        this.toString = function () {
            return this.rule + "\n [" + this.queryRules.join("\n") + "]" + "\n [" + this.answerRules.join("\n") + "]\n";
        };
        this.rule = rule;
        this.queryRules = queryRules;
        this.answerRules = answerRules;
        this.sipsRules = sipsRules;
    }
}
exports.MagicRule = MagicRule;
"use strict";
class MagicSet {
    constructor(ruleDb, directGraph) {
        this.toString = function () {
            return this.queryList.join('.\n') + '.' + '\n\n' + this.answerList.join('.\n') + '.';
        };
        this.SipsRulestoString = function () {
            var str = "";
            console.log("SipsRulestoString:" + this.sipsList);
            for (var i = 0; i < this.sipsList.length; i++) {
                str += this.sipsList[i].toStringWithAdorment() + "\n";
            }
            return str;
        };
        this.ruleDb = ruleDb;
        this.directGraph = directGraph;
    }
    getCombinations(chars) {
        var result = [];
        var f = function (prefix, chars) {
            for (var i = 0; i < chars.length; i++) {
                result.push(prefix + chars[i]);
                f(prefix + chars[i], chars.slice(i + 1));
            }
        };
        f('', chars);
        return result;
    }
    generateMS() {
        this.answerList = [];
        this.queryList = [];
        this.sipsList = [];
        this.magicSet = [];
        for (let rule of this.ruleDb.getRules()) {
            var magicRule = new MagicRule(rule, [], [], []);
            var listAdormentStr = "";
            for (var i = 1; i <= rule.head.listTerm.length; i++) {
                listAdormentStr += '' + i;
            }
            var listAdorment = this.getCombinations(listAdormentStr);
            listAdorment.push('0');
            for (let adorment of listAdorment) {
                var unifiedVar = new Map();
                var sipsBody;
                var sipsObj = new SIPS(rule, adorment);
                if (adorment != '0') {
                    sipsBody = sipsObj.optimalRule();
                    this.sipsList.push(sipsObj.sipsRule);
                    magicRule.sipsRules.push(sipsObj.sipsRule);
                }
                else {
                    sipsBody = sipsObj.optimalRuleWithoutAdorment(this.directGraph);
                    this.sipsList.push(sipsObj.sipsRule);
                    magicRule.sipsRules.push(sipsObj.sipsRule);
                }
                var listTermQuery = [];
                if (adorment != '0')
                    for (var i = 0; i < adorment.length; i++) {
                        var term = rule.head.listTerm[adorment[i] - 1];
                        listTermQuery.push(term);
                        unifiedVar.set(term.name, true);
                    }
                var answerHead = new AnswerLiteral(rule.head.relation, rule.head.listTerm, rule.head.isPositive, adorment);
                var querySeed = new QueryLiteral(rule.head.relation, listTermQuery, adorment);
                var listLiteral = [];
                listLiteral.push(querySeed);
                for (let bodyliteral of sipsBody) {
                    var newLiteral;
                    var queryLiteral;
                    if (this.directGraph.has(bodyliteral.relation)) {
                        var listTermQueryLiteral = [];
                        var adormentQuery = "";
                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {
                            var term = bodyliteral.listTerm[i];
                            if (unifiedVar.has(term.name)) {
                                listTermQueryLiteral.push(term);
                                adormentQuery += "" + (i + 1);
                            }
                        }
                        newLiteral = new AnswerLiteral(bodyliteral.relation, bodyliteral.listTerm, bodyliteral.isPositive, adormentQuery);
                        queryLiteral = new QueryLiteral(bodyliteral.relation, listTermQueryLiteral, adormentQuery);
                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {
                            var term = bodyliteral.listTerm[i];
                            unifiedVar.set(term.name, true);
                        }
                        var newlistLiteral = listLiteral.slice();
                        var MSQueryRule = new Rule(queryLiteral, newlistLiteral);
                        this.queryList.push(MSQueryRule);
                        magicRule.queryRules.push(MSQueryRule);
                    }
                    else {
                        newLiteral = bodyliteral;
                        for (var i = 0; i < bodyliteral.listTerm.length; i++) {
                            var term = bodyliteral.listTerm[i];
                            unifiedVar.set(term.name, true);
                        }
                    }
                    listLiteral.push(newLiteral);
                }
                var MSAnswerRule = new Rule(answerHead, listLiteral);
                this.answerList.push(MSAnswerRule);
                magicRule.answerRules.push(MSAnswerRule);
            }
            this.magicSet.push(magicRule);
        }
    }
}
exports.MagicSet = MagicSet;
//# sourceMappingURL=datalog.js.map