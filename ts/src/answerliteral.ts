"use strict";
class AnswerLiteral extends Literal {

    adorment: string;

    constructor(relation, listTerm, isPositive, adorment) {
        super(relation, listTerm, isPositive);
        this.adorment = adorment;
    }

    toString = function() {

        if (this.isPositive)
            return 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
        else
            return " not " + 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
    };

}