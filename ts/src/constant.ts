"use strict";
class Constant extends Term {

	value: string;

	constructor(value) {
		super("Constant");
		this.value = value;
		this.isConstant = true;
	}

	toString = function() {
		return this.value;
	};
}