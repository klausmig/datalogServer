
export { Term };
export { Constant };
export { Variable };
export { Match };
export { Literal };
export { Rule };
export { SipsRule };
export { Database };
export { log };
export { lexer };
export { DependencyGraph };
export { Parser };
export { AnswerLiteral };
export { QueryLiteral };
export { SIPS };
export { MagicRule };
export { MagicSet };
