"use strict";
class SIPS {

    adorment: string;
    rule: Rule;
    sipsRule: SipsRule;


    constructor(rule, adorment) {

        this.rule = rule;
        this.adorment = adorment;

    }

    optimalRule(): any {

        var queryTerm = new Map();

        for (var i = 0; i < this.adorment.length; i++) {
            //console.log("terms:" + this.rule.head.listTerm + " adorment:" + this.adorment);
            var index = this.adorment[i];
            var term = < Variable > this.rule.head.listTerm[ < any > index - 1];
            // console.log(" add term:" + term + " adorment:" + this.adorment);
            queryTerm.set(term.name, term);
        }
        //console.log("queryTerm:" + JSON.stringify([...queryTerm]));
        var newBody = [];
        var newNegativeBody = [];

        var unboundBody = [];
        var unboundNegativeBody = [];

        // console.log("processing rule:" + this.rule);

        for (let bodyliteral of this.rule.body) {

            var totalBind = 0;

            for (let term of bodyliteral.listTerm) {
                var variable = < Variable > term;
                if (queryTerm.has(variable.name)) {
                    totalBind++;
                    // console.log("variable.name:" + variable.name + " variable: " + variable);
                }
            }
            bodyliteral.totalBind = totalBind;
           // console.log("bodyliteral:" + bodyliteral + " bodyliteral.totalBind: " + bodyliteral.totalBind);

            if (totalBind == 0)
                if (bodyliteral.isPositive) {
                    unboundBody.push(bodyliteral);
                } else
                    unboundNegativeBody.push(bodyliteral);
            else
            if (bodyliteral.isPositive) {
                newBody.push(bodyliteral);
            } else
                newNegativeBody.push(bodyliteral);
        }
        var sortedSips: Literal[] = newBody.sort((n1, n2) => n2.totalBind - n1.totalBind);


        if (unboundBody.length > 0) {
            var unifiedTerm = new Map();

            for (let boundBodyliteral of sortedSips) {
                for (let term of boundBodyliteral.listTerm) {
                    var variable = < Variable > term;
                    unifiedTerm.set(variable.name, true);
                }
            }

            //console.log("unifiedTerm" + [...unifiedTerm]);

            var newBoundBody = [];
            for (let unboundBodyliteral of unboundBody) {

                var totalBind = 0;

                for (let term of unboundBodyliteral.listTerm) {
                    var variable = < Variable > term;
                    if (unifiedTerm.has(variable.name)) {
                        totalBind++;
                        // console.log("variable.name:" + variable.name + " variable: " + variable);
                    }
                }

                unboundBodyliteral.totalBind = totalBind;
                //console.log("unboundBodyliteral:" + unboundBodyliteral + " unboundBodyliteral.totalBind: " + unboundBodyliteral.totalBind);
                newBoundBody.push(unboundBodyliteral);
            }

            var sortedUnboundSips: Literal[] = newBoundBody.sort((n1, n2) => n2.totalBind - n1.totalBind);

            //console.log("sortedUnboundSips" + sortedUnboundSips);

            sortedSips = sortedSips.concat(sortedUnboundSips);
        }

        if (newNegativeBody.length > 0) {
            var sortedNegativeSips: Literal[] = newNegativeBody.sort((n1, n2) => n2.totalBind - n1.totalBind);

           // console.log("neg SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedNegativeSips);
            sortedSips = sortedSips.concat(sortedNegativeSips);

           }

        if (unboundNegativeBody.length > 0) {
            var sortedNegativeSips: Literal[] = unboundNegativeBody.sort((n1, n2) => n2.totalBind - n1.totalBind);
            //console.log("unneg SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedNegativeSips);
            sortedSips = sortedSips.concat(sortedNegativeSips);
        }

        //console.log("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        log.push("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);

        this.sipsRule = new SipsRule(this.rule.head, sortedSips, this.rule.head.relation + this.adorment);
        //console.log("sipsrule:" + this.sipsRule);
        return sortedSips;

    };

    optimalRuleWithoutAdorment(directGraph): any { //adorment 0


        //console.log("queryTerm:" + JSON.stringify([...queryTerm]));
        var newBaseBody = [];
        var newDerivatedBody = [];
        var newNegativeBody = [];

        // console.log("processing rule:" + this.rule);

        for (let bodyliteral of this.rule.body) {

            bodyliteral.totalBind = bodyliteral.listTerm.length;

            if (bodyliteral.isPositive) {
                if (directGraph.has(bodyliteral.relation)) { //It is a derivated relation
                    newDerivatedBody.push(bodyliteral);
                } else //It is a base relation
                    newBaseBody.push(bodyliteral);
            } else
                newNegativeBody.push(bodyliteral);


        }
        var sortedSips: Literal[] = newBaseBody.sort((n1, n2) => n1.totalBind - n2.totalBind);

        if (newDerivatedBody.length > 0) {
            var sortedDerivatedSips: Literal[] = newDerivatedBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedDerivatedSips);

        }

        if (newNegativeBody.length > 0) {
            var sortedNegativeSips: Literal[] = newNegativeBody.sort((n1, n2) => n1.totalBind - n2.totalBind);
            sortedSips = sortedSips.concat(sortedNegativeSips);

        }

       // console.log("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);
        log.push("SIPS Adorment:" + this.rule.head.relation + this.adorment + ":" + this.rule.head + ":-" + sortedSips);

        this.sipsRule = new SipsRule(this.rule.head, sortedSips, this.rule.head.relation);
       // console.log("sipsrule:" + this.sipsRule);
        return sortedSips;

    };

    toString = function() {
        return 'answer_' + this.relation + '(' + this.listTerm.join(',') + ')';
    };

}