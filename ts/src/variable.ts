"use strict";

class Variable extends Term {

    name: string;

    constructor(name) {
        super('Variable');
        this.name = name;

    }

    match = function(other) {
        var bindings = new Map;
        if (this !== other) {
            bindings.set(this, other);
        }
        return bindings;
    };

    substitute = function(bindings) {
        var value = bindings.get(this);
        if (value) {
            return value;
        }
        return this;
    };

    toString = function() {
        return this.name;
    };
}